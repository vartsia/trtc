//
// RayGISUnitTestsCh4.swift
// RayGISUnitTests
//
// Created by Juhani Nikumaa on 12/04/2019
//
// MIT License
// Copyright (c) 2019 Juhani Nikumaa
// vartsia.net
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.


import XCTest

import RGCore
import simd


class RayGISUnitTestsCh4: XCTestCase {

    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    
    /**
     Chapter 4.
     1st test case.
     */
    func testMultipltyPointByTranslationMatrix() {
        // Given
        let transform = newRGTranslationMatrix(x: 5, y: -3, z: 2)
        // And
        let p = newRGPoint(x: -3, y: 4, z: 5)
        
        // When
        let tp = transform * p
        
        // Then
        XCTAssertEqual(tp.isEqualTo(newRGPoint(x: 2, y: 1, z: 7)), true)
    }

    
    /**
     Chapter 4.
     2nd test case.
     */
    func testMultipltyPointByInverseOFTranslationMatrix() {
        // Given
        let transform = newRGTranslationMatrix(x: 5, y: -3, z: 2)
        let inverseTransf = transform.inverse
        // And
        let p = newRGPoint(x: -3, y: 4, z: 5)
        
        // When
        let ip = inverseTransf * p
        
        // Then
        XCTAssertEqual(ip.isEqualTo(newRGPoint(x: -8, y: 7, z: 3)), true)
    }
    

    /**
     Chapter 4.
     3rd test case.
     Test that vectors are NOT affected by translation matrix.
     */
    func testMultiplyVectorByTranslationMatrix() {
        // Given
        let transform = newRGTranslationMatrix(x: 5, y: -3, z: 2)
        
        // And
        let v = newRGVector(x: -3, y: 4, z: 5)
        
        // When
        let tv = transform * v
        
        // Then
        XCTAssertEqual(tv.isEqualTo(newRGVector(x: -3, y: 4, z: 5)), true)
    }
    
    
    /**
     Chapter 4.
     4th test case.
     Test multiplying point by scaling matrix.
     */
    func testMultiplyPointByScalingMatrix() {
        // Given
        let transform = newRGScalingMatrix(x: 2, y: 3, z: 4)
        // And
        let p = newRGPoint(x: -4, y: 6, z: 8)
        
        // When
        let tp = transform * p
        
        // Then
        XCTAssertEqual(tp.isEqualTo(newRGPoint(x: -8, y: 18, z: 32)), true)
    }

    
    /**
     Chapter 4.
     5th test case.
     Test multiplying vector by scaling matrix.
     */
    func testMultiplyVectorByScalingMatrix() {
        // Given
        let transform = newRGScalingMatrix(x: 2, y: 3, z: 4)
        // And
        let v = newRGVector(x: -4, y: 6, z: 8)
        
        // When
        let tv = transform * v
        
        // Then
        XCTAssertEqual(tv.isEqualTo(newRGVector(x: -8, y: 18, z: 32)), true)
    }
    
    
    /**
     Chapter 4.
     6th test case.
     Test multiplying vector by inverse scaling matrix.
     */
    func testMultiplyVectorByInverseOfScalingMatrix() {
        // Given
        let transform = newRGScalingMatrix(x: 2, y: 3, z: 4)
        // And
        let v = newRGVector(x: -4, y: 6, z: 8)
        // And
        let invTransform = transform.inverse
        
        // When
        let tv = invTransform * v
        
        // Then
        XCTAssertEqual(tv.isEqualTo(newRGVector(x: -2, y: 2, z: 2)), true)
    }
    

    /**
     Chapter 4.
     7th test case.
     Test multiplying by negative value aka. reflection.
     */
    func testReflectionAcrossXAxis() {
        // Given
        let transform = newRGScalingMatrix(x: -1, y: 1, z: 1)
        // And
        let p = newRGVector(x: 2, y: 3, z: 4)
        
        // When
        let tv = transform * p
        
        // Then
        XCTAssertEqual(tv.isEqualTo(newRGVector(x: -2, y: 3, z: 4)), true)
    }

    
    /**
     Chapter 4.
     8th test case.
     Test rotation around x-axis.
     */
    func testRotationAroundX() {
        // Given
        let p = newRGPoint(x: 0, y: 1, z: 0)
        // and
        let halfQuarter = newRGRotationX(Double.pi / 4)
        // and
        let fullQuarter = newRGRotationX(Double.pi / 2)
        
        // When
        let halfRot = halfQuarter * p
        let fullRot = fullQuarter * p
        
        // Then
        XCTAssertEqual(halfRot.isEqualTo(newRGPoint(x: 0, y: Double(2.0).squareRoot()/2.0, z: Double(2.0).squareRoot() / 2.0 )), true)
        XCTAssertEqual(fullRot.isEqualTo(newRGPoint(x: 0, y: 0, z: 1)), true)
    }
    
    
    /**
     Chapter 4.
     9th test case.
     Test inverse rotation around x-axis.
     */
    func testInverseRotationAroundX() {
        // Given
        let p = newRGPoint(x: 0, y: 1, z: 0)
        // and
        let halfQuarter = newRGRotationX(Double.pi / 4)
        let invHalf = halfQuarter.inverse
        
        // When
        let halfInvRot = invHalf * p
        
        // Then
        XCTAssertEqual(halfInvRot.isEqualTo(newRGPoint(x: 0, y: Double(2.0).squareRoot()/2.0, z: -Double(2.0).squareRoot() / 2.0 )), true)
    }

    
    /**
     Chapter 4.
     10th test case.
     Test rotation around y-axis.
     */
    func testRotationAroundY() {
        // Given
        let p = newRGPoint(x: 0, y: 0, z: 1)
        // and
        let halfQuarter = newRGRotationY(Double.pi / 4)
        // and
        let fullQuarter = newRGRotationY(Double.pi / 2)
        
        // When
        let halfRot = halfQuarter * p
        // and
        let fullRot = fullQuarter * p
        
        // Then
        XCTAssertEqual(halfRot.isEqualTo(newRGPoint(x: Double(2.0).squareRoot()/2.0, y: 0, z: Double(2.0).squareRoot()/2.0)), true)
        XCTAssertEqual(fullRot.isEqualTo(newRGPoint(x: 1, y: 0, z: 0)), true)
    }
    
    
    /**
     Chapter 4.
     11th test case.
     Test rotation around y-axis.
     */
    func testRotationAroundZ() {
        // Given
        let p = newRGPoint(x: 0, y: 1, z: 0)
        // and
        let halfQuarter = newRGRotationZ(Double.pi / 4)
        // and
        let fullQuarter = newRGRotationZ(Double.pi / 2)
        
        // When
        let halfRot = halfQuarter * p
        // and
        let fullRot = fullQuarter * p
        
        // Then
        XCTAssertEqual(halfRot.isEqualTo(newRGPoint(x: -Double(2.0).squareRoot()/2.0, y: Double(2.0).squareRoot()/2.0, z: 0)), true)
        XCTAssertEqual(fullRot.isEqualTo(newRGPoint(x: -1, y: 0, z: 0)), true)
    }
    
    
    /**
     Chapter 4.
     12th test case.
     Test shearing x in propotion to y.
     */
    func testShearingXInPropotionToY() {
        // Given
        let transform = newRGShearing(1, 0, 0, 0, 0, 0)
        
        // and
        let p = newRGPoint(x: 2, y: 3, z: 4)
        
        // When
        let sharedP = transform * p
        
        XCTAssertEqual(sharedP.isEqualTo(newRGPoint(x: 5, y: 3, z: 4)), true)
        
    }
    
    
    /**
     Chapter 4.
     13th test case.
     Test shearing x in propotion to z.
     */
    func testShearingXInPropotionToZ() {
        // Given
        let transform = newRGShearing(0, 1, 0, 0, 0, 0)
        
        // and
        let p = newRGPoint(x: 2, y: 3, z: 4)
        
        // When
        let sharedP = transform * p
        
        XCTAssertEqual(sharedP.isEqualTo(newRGPoint(x: 6, y: 3, z: 4)), true)
        
    }
    
    
    /**
     Chapter 4.
     14th test case.
     Test shearing y in propotion to x.
     */
    func testShearingYInPropotionToX() {
        // Given
        let transform = newRGShearing(0, 0, 1, 0, 0, 0)
        
        // and
        let p = newRGPoint(x: 2, y: 3, z: 4)
        
        // When
        let sharedP = transform * p
        
        XCTAssertEqual(sharedP.isEqualTo(newRGPoint(x: 2, y: 5, z: 4)), true)
        
    }
    
    
    /**
     Chapter 4.
     15th test case.
     Test shearing y in propotion to z.
     */
    func testShearingYInPropotionToZ() {
        // Given
        let transform = newRGShearing(0, 0, 0, 1, 0, 0)
        
        // and
        let p = newRGPoint(x: 2, y: 3, z: 4)
        
        // When
        let sharedP = transform * p
        
        XCTAssertEqual(sharedP.isEqualTo(newRGPoint(x: 2, y: 7, z: 4)), true)
        
    }
    
    
    /**
     Chapter 4.
     16th test case.
     Test shearing z in propotion to x.
     */
    func testShearingZInPropotionToX() {
        // Given
        let transform = newRGShearing(0, 0, 0, 0, 1, 0)
        
        // and
        let p = newRGPoint(x: 2, y: 3, z: 4)
        
        // When
        let sharedP = transform * p
        
        XCTAssertEqual(sharedP.isEqualTo(newRGPoint(x: 2, y: 3, z: 6)), true)
        
    }
    
    
    /**
     Chapter 4.
     17th test case.
     Test shearing z in propotion to y.
     */
    func testShearingZInPropotionToY() {
        // Given
        let transform = newRGShearing(0, 0, 0, 0, 0, 1)
        
        // and
        let p = newRGPoint(x: 2, y: 3, z: 4)
        
        // When
        let sharedP = transform * p
        
        XCTAssertEqual(sharedP.isEqualTo(newRGPoint(x: 2, y: 3, z: 7)), true)
        
    }
    
    
    /**
     Chapter 4.
     18th test case.
     Test individual transfomations in sequence.
     */
    func testIndividualTransformations() {
        // Given
        let p = newRGPoint(x: 1, y: 0, z: 1)
        let A = newRGRotationX(Double.pi / 2)
        let B = newRGScalingMatrix(double3(x: 5, y: 5, z: 5))
        let C = newRGTranslationMatrix(x: 10, y: 5, z: 7)
        
        // Apply rotation first
        
        // When
        let p2 = A * p
        // Then
        XCTAssertEqual(p2.isEqualTo(newRGPoint(x: 1, y: -1, z: 0)), true)
        
        // When
        let p3 = B * p2
        // Then
        XCTAssertEqual(p3.isEqualTo(newRGPoint(x: 5, y: -5, z: 0)), true)
        
        // When
        let p4 = C * p3
        // Then
        XCTAssertEqual(p4.isEqualTo(newRGPoint(x: 15, y: 0, z: 7)), true)
    }
    
    
    /**
     Chapter 4.
     19th test case.
     Test chained transfomations.
     */
    func testChainedTransformations() {
        // Given
        let p = newRGPoint(x: 1, y: 0, z: 1)
        let A = newRGRotationX(Double.pi / 2)
        let B = newRGScalingMatrix(double3(x: 5, y: 5, z: 5))
        let C = newRGTranslationMatrix(x: 10, y: 5, z: 7)
        
        // When
        let T = C * B * A
        let pT = T * p

        // Then
        XCTAssertEqual(pT.isEqualTo(newRGPoint(x: 15, y: 0, z: 7)), true)
    }
    
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
