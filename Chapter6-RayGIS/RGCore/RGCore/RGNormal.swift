//
// RGNormal.swift
// RGCore
//
// Created by Juhani Nikumaa on 01/06/2019
//
// MIT License
// Copyright (c) 2019 Juhani Nikumaa
// vartsia.net
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.


import Foundation
import simd


/**
 Calculates the normal on the surface of a sphere.
 */
public func normalAt(_ s: RGSphere, _ point: RGTuple) -> RGTuple {
    let objectPoint = s.transform.inverse * point
    let objectNormal = objectPoint - newRGPoint(x: 0, y: 0, z: 0)
    var worldNormal = s.transform.inverse.transpose * objectNormal
    worldNormal.w = 0.0
    return worldNormal.normalize()
}
