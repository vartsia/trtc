//
// RGSphere.swift
// RGCore
//
// Created by Juhani Nikumaa on 11/05/2019
//
// MIT License
// Copyright (c) 2019 Juhani Nikumaa
// vartsia.net
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.


import Foundation
import simd


public struct RGSphere {
    public let id = NSUUID.init().uuidString
    public var origin = newRGPoint(x: 0, y: 0, z: 0)
    public var radius: Double = 1.0
    public var transform: RGMatrix4x4 = RGMatrix4x4.identity()
    public var material: RGMaterial = newRGMaterial()
    
    public init() {
    }
}


/**
 Calculates the itersections points for a ray to a sphere.
 - Returns: Array of RGIntersections.
 */
public func intersectRGSphere(_ sphere: RGSphere, ray: RGRay) -> [RGIntersection] {
    var t: [RGIntersection] = []
    
    let tRay = transformRGRay(ray, matrix: simd_inverse(sphere.transform))
    
    /*
     The vector form the sphere's center, to the ray origin
     # Remember: the sphere is centered at the world origin
     */
    let p = newRGPoint(x: 0, y: 0, z: 0)
    
    let shpere_to_ray = tRay.origin - p
    
    let a = simd_dot(tRay.direction, tRay.direction)
    
    let b = Double(2.0) * simd_dot(tRay.direction, shpere_to_ray)

    let c = simd_dot(shpere_to_ray, shpere_to_ray) - Double(1.0)
    
    let discriminant = pow(Double(b), 2.0) - (Double(4.0) * a * c)

    if discriminant < Double(0.0) {
        return t
    }
    
    let t1 = (-b - sqrt(discriminant)) /  (Double(2.0) * a)
    let t2 = (-b + sqrt(discriminant)) /  (Double(2.0) * a)

    t.append(RGIntersection(t: t1, object: sphere))
    t.append(RGIntersection(t: t2, object: sphere))
    
    return t
}
