//
// RGIntersection.swift
// RGCore
//
// Created by Juhani Nikumaa on 12/05/2019
//
// MIT License
// Copyright (c) 2019 Juhani Nikumaa
// vartsia.net
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.


import Foundation


/**
 Stores infromation of an object that was intersected at given location t.
 */
public struct RGIntersection {
    public var t: Double
    public var object: RGSphere
    
    public init(t: Double, object: RGSphere) {
        self.t = t
        self.object = object
    }
}


/**
 Generates and array of RGIntersections.
 Variable number of intersections could be given to function.
 Ex. let i = intersectinonsRG(i: inters1, inters2, inters3)
 */
public func intersectinonsRG(i: RGIntersection...) -> [RGIntersection] {
    var intersections: [RGIntersection] = []
    
    for intersection in i {
        intersections.append(intersection)
    }
    
    // Sort these here so the hits function can find the hit faster.
    return intersections.sorted(by: {
        $0.t < $1.t
    })
}


/**
 Hit will be the first intersection where t is positive.
 If t is negative the intersection is behind the origin.
 */
public func hitRG(intersections: [RGIntersection]) -> RGIntersection? {
    for i in intersections {
        if i.t > 0 {
            return i
        }
    }
    return nil
}
