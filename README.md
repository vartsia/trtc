TRTC - The Ray Tracing Challenge
===========================
This project is a result of my journey through the book **The Ray Tracing Challenge** written by **Jamis Buck**.
Great book so far - I really recommend. All the two and a half chapters I have read so far anyway! :)
The journey could be read at my [@Vartsia blog](http://www.vartsia.net).

The source code here is structured so that there is an independent folder based on every chapter of the book. 
Every chapter based folder contains all you need to follow the matching blog post. 

The blog is not trying to be a step by step tutorial.
It's more like a travelogue trhoug the book and it will bring forth the challenges I had and the mistakes I've made.
Any how by studying the code from chapter to cahpter it's easy follow the progress see the changes in the code.
If you are a novice programmer you might find something usefull and if you are a pro you probably found something funny :)