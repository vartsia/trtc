//
//  RGShape.swift
//  RGCore
//
//  Created by Juhani Nikumaa on 27/10/2019.
//
// MIT License
// Copyright (c) 2019 Juhani Nikumaa
// vartsia.net
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import Foundation
import simd


public protocol RGShape {
    var id: String { get }
    
    // Chapter 9. Planes
    // List of the things that are common to all shapes:
    
    // 1st item on the list.
    // All shapes have transformation matrix.
    // Identity matrix should be the default value.
    var transform: RGMatrix4x4 { get set }
    
    // 2nd item on the list.
    // Material should have default value. Use newRGMaterial()
    var material: RGMaterial { get set }
    
    // 3rd item on the list.
    // Since intersect function needs to work for all shpae types, every shape should implement a local version of intersect function.
    
    func localIntersect(tRay: RGRay) -> [RGIntersection]
    
    /*
        4th itme on the list.
        When computing the normal vector, all shapes need to first convert the point to object space,
        multiplying it by the inverse of the shape’s transfor- mation matrix. Then, after computing the normal
        they must transform it by the inverse of the transpose of the transformation matrix,
        and then normalize the resulting vector before returning it.
        
        For example the RGSphere implements this method as described above.
     */
    func localNormalAt(point: RGTuple) -> RGTuple
    
}
