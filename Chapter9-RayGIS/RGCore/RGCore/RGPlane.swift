//
//  RGPlane.swift
//  RGCore
//
//  Created by Juhani Nikumaa on 02/12/2019.
//  Copyright © 2019 Juhani Nikumaa. All rights reserved.
//

import Foundation
import simd


public struct RGPlane: Equatable, RGShape {
    public let id = NSUUID.init().uuidString
    public var origin = newRGPoint(x: 0, y: 0, z: 0)
    public var transform: RGMatrix4x4 = RGMatrix4x4.identity()
    public var material: RGMaterial = newRGMaterial()
    
    public init() {
    }
    
    public func localIntersect(tRay: RGRay) ->  [RGIntersection] {
        
        if abs(tRay.direction.y) < Double.eps() {
            return []
        }
        
        
        let t = -tRay.origin.y / tRay.direction.y
        let r = RGIntersection(t: t, object: self)
        return [r]
    
    }

    
   public func localNormalAt(point: RGTuple) -> RGTuple {
        return newRGVector(x: 0, y: 1, z: 0)
    }
    
    
}
