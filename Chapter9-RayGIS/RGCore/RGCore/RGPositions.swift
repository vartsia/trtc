//
// File.swift
// RGCore
//
// Created by Juhani Nikumaa on 31/03/2019
//
// MIT License
// Copyright (c) 2019 Juhani Nikumaa
// vartsia.net
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.


import Foundation


/**
 RGPosition is an enum type used when index is needed.
 It's generic type to be used when ever index is limit
 from 0 to 2. Ex. in 3x3 matrix or double3 vector.
 */
public enum RGPosition3: Int {
    case First
    case Second
    case Third
}


/**
 RGPosition is type used when index is needed.
 It's generic type to be used when ever index is limit
 from 0 to 3. Ex. in 4x4 matrix or simd_double4 vector.
 */
public enum RGPosition4: Int {
    case First
    case Second
    case Third
    case Fourth
}
