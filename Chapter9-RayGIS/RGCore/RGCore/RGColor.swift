//
//  RGColor.swift
//  RGCore
//
//  Created by Juhani Nikumaa on 27/03/2019.
//
//  MIT License
//  Copyright (c) 2019 Juhani Nikumaa
//  vartsia.net
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import Foundation
import simd

/**
 Chapter 2.
 */
public typealias RGColor = simd_double3


/**
 RGColor extension to get amd set inner values using typical channel names:
 red, green and blue.
 */
public extension RGColor {
    var red: Double {
        get {
            return self.x
        }
        set {
            self.x = newValue
        }
    }
    
    
    var green: Double {
        get {
            return self.y
        } set {
            self.y = newValue
        }
    }
    
    
    var blue: Double {
        get {
            return self.z
        }
        set {
            self.z = newValue
        }
    }
    
    var R: Int {
        get {
            let r = self.red
            if r < 0.0 {
                return 0
                }
            else if r > 1.0 {
                return 255
                }
            else {
                return Int(r * Double(255).rounded())
                }
        }
    }
    
    
    var G: Int {
        get {
            let g = self.green
            if g < 0.0 {
                return 0
            }
            else if g > 1.0 {
                return 255
            }
            else {
                return Int(g * Double(255).rounded() )
            }
        }
    }
    
    
    var B: Int {
        get {
            let b = self.blue
            if b < 0.0 {
                return 0
            }
            else if b > 1.0 {
                return 255
            }
            else {
                return Int(b * Double(255).rounded())
            }
        }
    }
    
    static func ==(_ lT: RGColor, _ rT: RGColor) -> Bool {
        if  lT.x.isEqualTo(rT.x) &&
            lT.y.isEqualTo(rT.y) &&
            lT.z.isEqualTo(rT.z) {
            return true
        }
        else {
            return false
        }
    }
}


/**
 Creates new color initializing it to balck.
 All channels to 0.0
 */

public func newRGColorBlack() -> RGColor {
    return newRGColor(red: 0.0, green: 0.0, blue: 0.0)
}


/**
 Create new RGColor with white.
 */
public func newRGColorWhite() -> RGColor {
    return newRGColor(red: 1.0, green: 1.0, blue: 1.0)
}


/**
 Factory function to create a new RGColor.
 */
public func newRGColor(red: Double, green: Double, blue: Double) -> RGColor {
    return RGColor(x: red, y: green, z: blue)
}


/**
 RGColor equality function that uses Double extension isEqualTo
 which is based on the epsilon comparison.
 */
public func equalColors(_ lT: RGColor, _ rT: RGColor) -> Bool {
    if  lT.x.isEqualTo(rT.x) &&
        lT.y.isEqualTo(rT.y) &&
        lT.z.isEqualTo(rT.z) {
        return true
        }
    else {
        return false
        }
}


/**
 Ties up the functions prepareRGComputation(), newRGShadeHit() and intersectRGWorld()
 */
public func RGColorAt(world: RGWorld, ray: RGRay) -> RGColor {
    // 1. Call intersect_world to find the intersections of the given ray with the given world.
    let intersections = intersectRGWorld(world: world, ray: ray)
    
    // 2. Find the hit from the resulting intersections.
    // Return black if there is NO hit.
    guard let hit = hitRG(intersections: intersections) else {
        // 3. Return the color black if there is no such intersection.
        return newRGColorBlack()
    }
    
    // 4. Otherwise, precompute the necessary values with prepare_computations.
    let comp = prepareRGComputation(intersection: hit, ray: ray)
    
    // 5. Finally, call shade_hit to find the color at the hit.
    return newRGShadeHit(world: world, computations: comp)
}

