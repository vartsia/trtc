//
//  RGCanvas.swift
//  RGCore
//
//  Created by Juhani Nikumaa on 28/03/2019.
//
//  MIT License
//  Copyright (c) 2019 Juhani Nikumaa
//  vartsia.net
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.


import Foundation
import simd

// Coordinate system used for
// for images.
//           y ^ height
//           y
//           y
//           y
//           y
// x x x x x o x x x x x > width
//           y
//           y
//           y
//           y
//           y
//           y
//           y

/**
 Typealiasign int2 type vector to RGPostion type for convinience.
 */
public typealias RGPosition = SIMD2<UInt32>


public struct RGSize {
    public var width: Int
    public var height: Int
}


/**
 Enum for different ASCII image formats.
 */
public enum ASCIIImageFormats {
    case PPM
}


/**
 This is canvas type.
 Width is pixels in x direction => horizontal.
 Height is pixels in y direction => vertical.
 */
public struct RGCanvas {
    public var size: RGSize
    private var grid: [[RGColor]]
    
    
    init(size: RGSize) {
        self.size = size
        self.grid = Array(repeating: Array(repeating: newRGColorBlack(), count: size.height), count: size.width)
    }
    
    /**
     
     */
    public mutating func writePixelAt(_ position: RGPosition, color: RGColor) -> Bool {
        if position.x < self.size.width && position.y < self.size.height {
            let x = Int(position.x)
            let y = Int(position.y)
            self.grid[x][y] = color
            return true
            }
        return false
    }
    
    
    /**
     Sets new base color for the canvas.
     Writes the new color into every cell in the grid.
     */
    public mutating func setNewBaseColor(_ color: RGColor) {
        for x in 0..<self.size.width {
            for y in 0..<self.size.height {
                self.grid[x][y] = color
            }
        }
    }
    
    
    /**
     - Returns: RGPosition for the first pixel that is not equal to given color.
                If no such pixel is found nil is returned.
     */
    public func findPositionOfUnequal(_ color: RGColor) -> RGPosition? {
        for x in 0..<self.size.width {
            for y in 0..<self.size.height {
                if !equalColors(self.grid[x][y], color) {
                    return RGPosition(x: UInt32(x), y: UInt32(y))
                    }
            }
        }
        return nil
    }
    
    
    /**
     Returns a color of a pixel on a canvas at given position.
     - Returns: RGColor value on a position given as input.
     */
    public func pixelAt(_ position: RGPosition) -> RGColor {
        let color = self.grid[position.x.toInt][position.y.toInt]
        return color
    }
    
    
    /**
     Creates header for ascii format.
     Format is asked based on ASCIIImageFormat enum type.
     - Returns: String containing properly formatted header data.
     */
    public func canvasHeaderForASCIIFormat(_ format: ASCIIImageFormats = .PPM) -> String {
        switch format {
        case .PPM:
            return "P3\n\(self.size.width) \(self.size.height)\n255"
            }
    }
}


/**
 Creates new canvas with given width and height.
 */
public func newRGCanvas(width: Int, height: Int) -> RGCanvas {
    return RGCanvas.init(size: RGSize(width: width, height: height))
}


/**
 Writes specified pixel into the canvas at given position.
 This is for convinience to follow the book.
 */
public func writePixelToRGCanvas(_ canvas: inout RGCanvas, position: RGPosition, color: RGColor) -> Bool {
    return canvas.writePixelAt(position, color: color)
}


/**
 Returs a String that represents the PPM ascii formated image file.
 */
public func canvasToPPMString(_ canvas: RGCanvas) -> String {
    var fileContent = canvas.canvasHeaderForASCIIFormat()
    fileContent.append("\n")
    var lineTmp = ""
    var pixel = ""
    for y in 0..<canvas.size.height {
        for x in 0..<canvas.size.width {
            let position = RGPosition(x: x.toUInt32, y: y.toUInt32)
            pixel = "\(canvas.pixelAt(position).R) \(canvas.pixelAt(position).G) \(canvas.pixelAt(position).B)"
            
            if (lineTmp.count + pixel.count + 2) > 70 {
                fileContent.append(contentsOf: lineTmp)
                fileContent.removeLast()
                fileContent.append("\n")
                lineTmp.removeAll()
                }
            
            lineTmp.append(contentsOf: pixel)
            lineTmp.append(" ")
            
        }
    }
    fileContent.append(contentsOf: lineTmp)
    fileContent.removeLast()
    fileContent.append("\n") // Last character should be newline to make format valid.
    return fileContent
}
