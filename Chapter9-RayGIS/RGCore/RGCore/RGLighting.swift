//
// RGLighting.swift
// RGCore
//
// Created by Juhani Nikumaa on 08/06/2019
//
// MIT License
// Copyright (c) 2019 Juhani Nikumaa
// vartsia.net
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.


import Foundation
import simd


public func newRGLighting(m: RGMaterial, p: RGTuple, l: RGPointLight, e: RGTuple, n: RGTuple, inShadow: Bool) -> RGColor {
    // combine the surface color with the light's color/intensity
    let effectiveColor = m.color * l.intensity
    
    // find the direction to the light source
    let lightv = (l.location - p).normalize()
    
    // compute the ambient contribution
    let ambient = effectiveColor * m.ambient
    
    
    // Since diffuce and specular are dependent on the ligh source
    // points in shadow should ignore these and return only ambient.
    if inShadow == true {
        return ambient
    }
    
    // lightDotNormal represents the cosine of the angel between the
    // light vector and the normal vector. A negative number means the
    // light is on the other side of teh surface.
    var diffuse: RGColor
    var specular: RGColor
    
    let lightDotNormal = dot(lightv, n)
    if lightDotNormal < 0.0 {
        diffuse = newRGColorBlack()
        specular = newRGColorBlack()
    } else {
        // compute the diffuse contribution
        diffuse = effectiveColor * m.diffuse * lightDotNormal
        
        // reflect_dot_eye represents the cosine of the angle between the
        // reflection vector and the eye vector. A negative number means the
        // light reflects away from the eye.
        let reflectv = newRGReflect(-lightv, n)
        let reflectDotEye = dot(reflectv, e)
        
        if reflectDotEye <= 0.0 {
            specular = newRGColorBlack()
        } else {
            let factor = pow(reflectDotEye, m.shininess)
            specular = l.intensity * m.specular * factor
        }
    }
    
    return ambient + diffuse + specular
}
