//
// RGTupleExtensio, z: self.ns.swift
// RGCore
//
// Created by Juhani Nikumaa on 09/04/2019
//
// MIT License
// Copyright (c) 2019 Juhani Nikumaa
// vartsia.net
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.


import Foundation
import simd


public extension simd_double3 {
    func isEqualTo(_ v: simd_double3) -> Bool {
        if self.x.isEqualTo(v.x) && self.y.isEqualTo(v.y) && self.z.isEqualTo(v.z) {
            return true
            }
        else {
            return false
            }
    }
    
    
    /**
     In simd we handle vectors as 1 dimensional column matrix.
     So the values x, y and z could be handeld as rows.
     x
     y
     z
     */
    func removeRow(_ row: RGPosition3) -> simd_double2 {
        switch row {
        case .First:
            return simd_double2(x: self.y, y: self.z)
        case .Second:
            return simd_double2(x: self.x, y: self.z)
        case .Third:
            return simd_double2(x: self.x, y: self.y)
            }
    }
}


public extension simd_double4 {
    func isEqualTo(_ v: simd_double4) -> Bool {
        if self.x.isEqualTo(v.x) && self.y.isEqualTo(v.y) && self.z.isEqualTo(v.z) && self.w.isEqualTo(v.w) {
            return true
        }
        else {
            return false
        }
    }
    
    
    /**
     In simd we handle vectors as 1 dimensional column matrix.
     So the values x, y and z could be handeld as rows.
     x
     y
     z
     */
    func removeRow(_ row: RGPosition4) -> simd_double3 {
        switch row {
        case .First:
            return simd_double3(x: self.y, y: self.z, z: self.w)
        case .Second:
            return simd_double3(x: self.x, y: self.z, z: self.w)
        case .Third:
            return simd_double3(x: self.x, y: self.y, z: self.w)
        case .Fourth:
            return simd_double3(x: self.x, y: self.y, z: self.z)
        }
    }
}
