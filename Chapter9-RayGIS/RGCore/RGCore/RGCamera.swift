//
//  RGCamera.swift
//  RGCore
//
//  Created by Juhani Nikumaa on 18/08/2019.
//  Copyright © 2019 Juhani Nikumaa. All rights reserved.
//

import Foundation


public class RGCamera {
    public var hSize: Int  {
        didSet {
            calcPixelSize()
        }
    }
    public var vSize: Int  {
        didSet {
            calcPixelSize()
        }
    }
    public var fOfView: Double {
        didSet {
            calcPixelSize()
        }
    }
    public var transformation: RGMatrix4x4
    public private(set) var halfWidth: Double = 0
    public private(set) var halfHeight: Double = 0
    public private(set) var pixelSize: Double = 1.0
    
    private func calcPixelSize() {
        let halfView = tan(fOfView / 2.0)
        let aspectRatio = hSize.d / vSize.d
        if aspectRatio >= 1 {
            halfWidth = halfView
            halfHeight = halfView / aspectRatio
        } else {
            halfWidth = halfView * aspectRatio
            halfHeight = halfView
        }
        
        pixelSize = (halfWidth * 2.0) / hSize.d
    }
    
    public init(hsize: Int, vsize: Int, fieldOfView: Double) {
        hSize = hsize
        vSize = vsize
        fOfView = fieldOfView
        transformation = RGMatrix4x4.identity()
        calcPixelSize()
    }
}
