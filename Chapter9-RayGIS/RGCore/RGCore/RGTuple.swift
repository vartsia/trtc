//
//  RGTuple.swift
//  RGCore
//
//  Created by Juhani Nikumaa on 27/03/2019.
//
//  MIT License
//  Copyright (c) 2019 Juhani Nikumaa
//  vartsia.net
// 
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
// 
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
// 
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import Foundation
import simd


/**
 Just give alias to the simd_double4 and
 we are ready to add our special needs on top of it.
 */
public typealias RGTuple = simd_double4


/**
 Convinience method for RGTuple to negate all elements.
 */
public extension RGTuple {
    mutating func negate() {
        self.x = -self.x
        self.y = -self.y
        self.z = -self.z
        self.w = -self.w
    }
    
    /**
     Convinience method so test code can partaly stay the same.
     */
    func magnitude() -> Double {
        return length(self)
    }
    
    /**
     Convinienve method to normalize tuple.
     */
    func normalize() -> RGTuple {
        return simd_precise_normalize(self)
    }
    
    
    func getRGPosition() -> RGPosition {
        let x = self.x.isLess(than: 0.0) ? 0 :UInt32(self.x.rounded())
        let y = self.y.isLess(than: 0.0) ? 0 :UInt32(self.y.rounded())
        return RGPosition(x: x, y: y)
    }
    
    func printTuple() {
        print("Tuple x = \(self.x), y = \(self.y), z = \(self.z), w = \(self.w)")
    }
}


/**
 - Returns: true if RGTuple is point.
 */
public func isRGPoint(_ t: RGTuple) -> Bool {
    return t.w.isEqual(to: 1.0)
}


/**
 - Returns: true if RGTuple is vector.
 */
public func isRGVector(_ t: RGTuple) -> Bool {
    return t.w.isEqual(to: 0.0)
}


/**
 Function to construct new RGPoint.
 */
public func newRGPoint(x: Double, y: Double, z: Double) -> RGTuple {
    return RGTuple(x: x, y: y, z: z, w: 1.0)
}


/**
 Function to construct new RGVector.
 */
public func newRGVector(x: Double, y: Double, z: Double) -> RGTuple {
    return RGTuple(x: x, y: y, z: z, w: 0.0)
}


/**
 This is needed to implement separatelly so we can use the Double equality function
 that uses epsilon to set the precision of comparison.
 */
public func equalTuples(_ lT: RGTuple, _ rT: RGTuple) -> Bool {
    if  lT.x.isEqualTo(rT.x) &&
        lT.y.isEqualTo(rT.y) &&
        lT.z.isEqualTo(rT.z) &&
        lT.w.isEqualTo(rT.w) {
        return true
        }
    else {
        return false
        }
}


/**
 We need to keep the rules how vectors and points can be added or subtracted.
 These operator overloading functions are created to obey those rules metntioned in Chapter 1.
 */


/**
 - Returns: A new RGTuple if adding is succesfull.
            The left operand if adding is not allowed.
 */
public func +(lT: RGTuple, rT: RGTuple) -> RGTuple {
    if (isRGVector(lT) && isRGPoint(rT) ) ||
        (isRGPoint(lT) && isRGVector(rT)) ||
        (isRGVector(lT) && isRGVector(rT)) {
        return RGTuple(x: lT.x + rT.x, y: lT.y + rT.y, z: lT.z + rT.z, w: lT.w + rT.w)
        }
    else {
        return lT
        }
}


/**
 - Returns: A new RGTuple if subtracting is succesfull.
 The left operand if adding is not allowed.
 */
public func -(lT: RGTuple, rT: RGTuple) -> RGTuple {
    if (isRGVector(lT) && isRGVector(rT) ) {
        // Result is vector
        return RGTuple(x: lT.x - rT.x, y: lT.y - rT.y, z: lT.z - rT.z, w: 0.0)
        }
    else if (isRGPoint(lT) && isRGPoint(rT)) {
        // Result is vector
        return RGTuple(x: lT.x - rT.x, y: lT.y - rT.y, z: lT.z - rT.z, w: 0.0)
        }
    else if (isRGPoint(lT) && isRGVector(rT)) {
        // Result is point
        return RGTuple(x: lT.x - rT.x, y: lT.y - rT.y, z: lT.z - rT.z, w: 1.0)
        }
    else {
        return lT
        }
}


/**
 There is no direct cross() function in simd for tuple that contains four elements.
 simd calls the datastructures vectors.
 We must implement this so it's easy to use corss with RGTuple.
 */
public func cross(lT: RGTuple, rT: RGTuple) -> RGTuple {
    let crs = cross(simd_double3(x: lT.x, y: lT.y, z: lT.z), simd_double3(x: rT.x, y: rT.y, z: rT.z))
    return newRGVector(x: crs.x, y: crs.y, z: crs.z)
}


public func dot(lT: RGTuple, rT: RGTuple) -> Double {
    let d = dot(lT, rT)
    return d
}
