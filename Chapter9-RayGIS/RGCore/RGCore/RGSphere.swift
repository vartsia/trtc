//
// RGSphere.swift
// RGCore
//
// Created by Juhani Nikumaa on 11/05/2019
//
// MIT License
// Copyright (c) 2019 Juhani Nikumaa
// vartsia.net
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.


import Foundation
import simd

// This is only for testing.
public var gRay: RGRay!

public struct RGSphere: RGShape {
    public let id = NSUUID.init().uuidString
    public var origin = newRGPoint(x: 0, y: 0, z: 0)
    public var radius: Double = 1.0
    public var transform: RGMatrix4x4 = RGMatrix4x4.identity()
    public var material: RGMaterial = newRGMaterial()
    
    public init() {
    }
    
    
    /**
     Calculates the normal on the surface of a sphere.
     */
    public func localNormalAt(point: RGTuple) -> RGTuple {
//        let objectPoint = self.transform.inverse * point
        var objectNormal = point - newRGPoint(x: 0, y: 0, z: 0)
        //var worldNormal = self.transform.inverse.transpose * objectNormal
        objectNormal.w = 0.0
        return objectNormal.normalize()
    }
    
    
    public func localIntersect(tRay: RGRay) ->  [RGIntersection] {
        
        // Set gRay so it can be checked in testing.
        gRay = tRay
        
        var t: [RGIntersection] = []
        
        /*
         The vector form the sphere's center, to the ray origin
         # Remember: the sphere is centered at the world origin
         */
        let p = newRGPoint(x: 0, y: 0, z: 0)
        
        let shape_to_ray = tRay.origin - p
        
        let a = simd_dot(tRay.direction, tRay.direction)
        
        let b = Double(2.0) * simd_dot(tRay.direction, shape_to_ray)
        
        let c = simd_dot(shape_to_ray, shape_to_ray) - Double(1.0)
        
        let discriminant = pow(Double(b), 2.0) - (Double(4.0) * a * c)
        
        if discriminant < Double(0.0) {
            return t
        }
        
        let t1 = (-b - sqrt(discriminant)) /  (Double(2.0) * a)
        let t2 = (-b + sqrt(discriminant)) /  (Double(2.0) * a)
        
        t.append(RGIntersection(t: t1, object: self))
        t.append(RGIntersection(t: t2, object: self))
        
        return t
    }
    
    /**
     Implements the Equatable protocol.
     We do not use the id since we just wan't to know if objects are equal in terms of data.
     To check if objects are the same just chek the id.
     */
    public static func == (l: RGSphere, r: RGSphere) -> Bool {
        return
            equalTuples(l.origin, r.origin) &&
            l.radius.isEqualTo(l.radius) &&
            l.transform.isEqualTo(r.transform) &&
            l.material == r.material
    }
}
