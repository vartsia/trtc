//
// RayGISUnitTestCh5.swift
// RayGISUnitTests
//
// Created by Juhani Nikumaa on 07/05/2019
//
// MIT License
// Copyright (c) 2019 Juhani Nikumaa
// vartsia.net
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.


import XCTest
import RGCore


class RayGISUnitTestCh5: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    
    func testRGRayCreation() {
        // Given
        let origin = newRGPoint(x: 1, y: 2, z: 3)
        let direction = newRGVector(x: 4, y: 5, z: 6)
        
        // When
        let ray = newRGRay(origin: origin, direction: direction)
        
        // Then
        XCTAssertEqual(ray.origin.isEqualTo(origin), true)
        XCTAssertEqual(ray.direction.isEqualTo(direction), true)
        
    }
    
    
    func testRGRayPosition() {
        // Given
        let o = newRGPoint(x: 2, y: 3, z: 4)
        let d = newRGVector(x: 1, y: 0, z: 0)
        let r = newRGRay(origin: o, direction: d)
        
        // Then
        let p1 = newRGPosition(ray: r, t: 0)
        let p2 = newRGPosition(ray: r, t: 1)
        let p3 = newRGPosition(ray: r, t: -1)
        let p4 = newRGPosition(ray: r, t: 2.5)
        
        XCTAssertEqual(p1.isEqualTo(newRGPoint(x: 2, y: 3, z: 4)), true)
        XCTAssertEqual(p2.isEqualTo(newRGPoint(x: 3, y: 3, z: 4)), true)
        XCTAssertEqual(p3.isEqualTo(newRGPoint(x: 1, y: 3, z: 4)), true)
        XCTAssertEqual(p4.isEqualTo(newRGPoint(x: 4.5, y: 3, z: 4)), true)
    }
    
    
    func testRGSphereIntersect() {
        // Given
        let r = newRGRay(origin: newRGPoint(x: 0, y: 0, z: -5), direction: newRGVector(x: 0, y: 0, z: 1))
        // and
        let s = RGSphere()
        let xs = intersect(s, ray: r)
        
        // Then
        XCTAssertEqual(xs.count == 2, true)
        XCTAssertEqual(xs[0].t.isEqualTo(Double(4)), true)
        XCTAssertEqual(xs[1].t.isEqualTo(Double(6)), true)
    }

    
    func testRGSphereIntersectAtTangent() {
        // Given
        let r = newRGRay(origin: newRGPoint(x: 0, y: 1, z: -5), direction: newRGVector(x: 0, y: 0, z: 1))
        // and
        let s = RGSphere()
        let xs = intersect(s, ray: r)
        
        // Then
        XCTAssertEqual(xs.count == 2, true)
        XCTAssertEqual(xs[0].t.isEqualTo(Double(5)), true)
        XCTAssertEqual(xs[1].t.isEqualTo(Double(5)), true)
    }
    
    
    func testRGSphereNotIntersect() {
        // Given
        let r = newRGRay(origin: newRGPoint(x: 0, y: 2, z: -5), direction: newRGVector(x: 0, y: 0, z: 1))
        // and
        let s = RGSphere()
        let xs = intersect(s, ray: r)
        
        // Then
        XCTAssertEqual(xs.count == 0, true)
    }
    
    
    func testRGSphereIntersectAtInside() {
        // Given
        let r = newRGRay(origin: newRGPoint(x: 0, y: 0, z: 0), direction: newRGVector(x: 0, y: 0, z: 1))
        
        // and
        let s = RGSphere()
        let xs = intersect(s, ray: r)
        
        // Then
        XCTAssertEqual(xs.count == 2, true)
        XCTAssertEqual(xs[0].t.isEqualTo(-1.0), true)
        XCTAssertEqual(xs[1].t.isEqualTo(1.0), true)
        
    }
    
    
    func testRGSphereIntersectBehind() {
        // Given
        let r = newRGRay(origin: newRGPoint(x: 0, y: 0, z: 5), direction: newRGVector(x: 0, y: 0, z: 1))
        
        // and
        let s = RGSphere()
        let xs = intersect(s, ray: r)
        
        // Then
        XCTAssertEqual(xs.count == 2, true)
        XCTAssertEqual(xs[0].t.isEqualTo(-6.0), true)
        XCTAssertEqual(xs[1].t.isEqualTo(-4.0), true)
    }
    
    
    func testRGIntersection() {
        // Given
        let s = RGSphere()
        
        // When
        let i = RGIntersection(t:Double(3.5), object: s)
        
        // Then
        XCTAssertEqual(i.t.isEqualTo(3.5), true)
        XCTAssertEqual(i.object.id == s.id, true)
    }
    
    
    func testRGIntersectionsAggregation() {
        // Given
        let s = RGSphere()
        let i1 = RGIntersection(t: 1.d, object: s)
        let i2 = RGIntersection(t: 2.d, object: s)
        
        // When
        let xs = intersectinonsRG(i: i1, i2)
        
        // Then
        XCTAssertEqual(xs.count == 2, true)
        XCTAssertEqual(xs[0].t.isEqualTo(1.d), true)
        XCTAssertEqual(xs[1].t.isEqualTo(2.d), true)
    }
    
    
    func testRGIntersectionObject() {
        // Given
        let r = newRGRay(origin: newRGPoint(x: 0, y: 0, z: -5), direction: newRGVector(x: 0, y: 0, z: 1))
        // and
        let s = RGSphere()
        
        // When
        let xs = intersect(s, ray: r)
        
        // Then
        XCTAssertEqual(xs.count == 2, true)
        XCTAssertEqual(xs[0].object.id == s.id, true)
        XCTAssertEqual(xs[1].object.id == s.id, true)
    }
    
    
    func testRGHits() {
        // Scenario: When all intersections have positive t
        // Given
        let s1 = RGSphere()
        let i1A = RGIntersection(t: 1.d, object: s1)
        let i2A = RGIntersection(t: 2.d, object: s1)
        let xsA = intersectinonsRG(i: i1A, i2A)
        
        // When
        let iA = hitRG(intersections: xsA)
        
        // Then
        XCTAssertEqual(iA!.object.id == i1A.object.id, true)
        
        
        // Scenario: When some intersections have negative t
        // Given
        let s2 = RGSphere()
        let i1B = RGIntersection(t: -1.d, object: s2)
        let i2B = RGIntersection(t:  1.d, object: s2)
        let xsB = intersectinonsRG(i: i1B, i2B)
        
        // When
        let iB = hitRG(intersections: xsB)
        
        // Then
        XCTAssertEqual(iB!.object.id == i2B.object.id, true)
        
        
        // Scenario: When all intersections have negative t
        // Given
        let s3 = RGSphere()
        let i1C = RGIntersection(t: -2.d, object: s3)
        let i2C = RGIntersection(t: -1.d, object: s3)
        let xsC = intersectinonsRG(i: i1C, i2C)
        
        // When
        let iC = hitRG(intersections: xsC)
        
        // Then
        XCTAssertEqual(iC == nil, true)
        
        
        // Scenario: The hit is always the lowes nonnegative intersection
        // Given
        let s4 = RGSphere()
        let i1D = RGIntersection(t:  5.d, object: s4)
        let i2D = RGIntersection(t:  7.d, object: s4)
        let i3D = RGIntersection(t: -3.d, object: s4)
        let i4D = RGIntersection(t:  2.d, object: s4)
        let xsD = intersectinonsRG(i: i1D, i2D, i3D, i4D)
        
        // When
        let iD = hitRG(intersections: xsD)
        
        // Then
        XCTAssertEqual(iD!.object.id == i4D.object.id, true)
        
    }
    
    
    func testRGRayTransformTranslation() {
        // Given
        let r = newRGRay(origin: newRGPoint(x: 1, y: 2, z: 3), direction: newRGVector(x: 0, y: 1, z: 0))
        // and
        let m = newRGTranslationMatrix(x: 3, y: 4, z: 5)
        
        // When
        let r2 = transformRGRay(r, matrix: m)
        
        // Then
        XCTAssertEqual(r2.origin == newRGPoint(x: 4, y: 6, z: 8), true)
        XCTAssertEqual(r2.direction == newRGVector(x: 0, y: 1, z: 0), true)
    }
    
    
    func testRGRayTransformScaling() {
        // Given
        let r = newRGRay(origin: newRGPoint(x: 1, y: 2, z: 3), direction: newRGVector(x: 0, y: 1, z: 0))
        // and
        let m = newRGScalingMatrix(x: 2, y: 3, z: 4)
        
        // When
        let r2 = transformRGRay(r, matrix: m)
        
        // Then
        XCTAssertEqual(r2.origin == newRGPoint(x: 2, y: 6, z: 12), true)
        XCTAssertEqual(r2.direction == newRGVector(x: 0, y: 3, z: 0), true)
    }
    
    
    func testRGSphereDefaultTransfomr() {
        // Given
        let s = RGSphere()
        
        // Then
        XCTAssertEqual(s.transform == RGMatrix4x4.identity(), true)
    }
    
    
    func testRGSetSpheresTransform() {
        // Given
        var s = RGSphere()
        let t = newRGTranslationMatrix(x: 2, y: 3, z: 4)
        s.transform = t
        // Then
        XCTAssertEqual(s.transform == t, true)
    }
    
    
    /**
     Intersecting a scaled sphere with a ray.
     */
    func testIntersectionScaledRGSphereWithRay() {
        // Given
        let r = newRGRay(origin: newRGPoint(x: 0, y: 0, z: -5), direction: newRGVector(x: 0, y: 0, z: 1))
        // and
        var s = RGSphere()
        
        // When
        s.transform = newRGScalingMatrix(x: 2, y: 2, z: 2)
        // and
        let xs = intersect(s, ray: r)
        
        // Then
        XCTAssertEqual(xs.count == 2, true)
        XCTAssertEqual(xs[0].t.isEqualTo(3.d), true)
        XCTAssertEqual(xs[1].t.isEqualTo(7.d), true)
    }
    
    
    func testIntersectionTranslatedRGSphereWithRay() {
        // Given
        let r = newRGRay(origin: newRGPoint(x: 0, y: 0, z: -5), direction: newRGVector(x: 0, y: 0, z: 1))
        // and
        var s = RGSphere()
        
        // When
        s.transform = newRGTranslationMatrix(x: 5, y: 0, z: 0)
        // and
        let xs = intersect(s, ray: r)
        
        // Then
        XCTAssertEqual(xs.count == 0, true)
    }
    
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
}
