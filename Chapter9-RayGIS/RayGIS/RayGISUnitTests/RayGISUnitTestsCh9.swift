//
//  RayGISUnitTestsCh9.swift
//  RayGISUnitTests
//
//  Created by Juhani Nikumaa on 27/10/2019.
//  Copyright © 2019 Juhani Nikumaa. All rights reserved.
//

import XCTest
import RGCore
import simd

class RayGISUnitTestsCh9: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testShape() {
        // Given
        // We use RGShpere here sice there is no need to creat separate struct for testing.
        // The tests are generic to all shapes so RGShpere should be a good caditade of being the test object
        // since it implements the RGShape.
        var s = RGSphere()
        
        // The default transformation
        XCTAssertEqual(s.transform == RGMatrix4x4.identity(), true)
        
        
        // Assing a transformation
        s.transform = newRGTranslationMatrix(x: 2, y: 3, z: 4)
        XCTAssertEqual(s.transform == newRGTranslationMatrix(x: 2, y: 3, z: 4), true)
        
        // The default material
        let m = s.material
        XCTAssertEqual(m == newRGMaterial(), true)
        
        // Assign a material
        var nm = newRGMaterial()
        nm.ambient = 1
        
        s.material = nm
        XCTAssertEqual(s.material == nm, true)
    }
    
    func testIntersectingAfterRefactorScale() {
        // Intersecting a scaled shape with a ray
        // Given
        let ray = newRGRay(origin: newRGPoint(x: 0, y: 0, z: -5), direction: newRGVector(x: 0, y: 0, z: 1))
        var s = RGSphere()
        
        // When
        s.transform = newRGScalingMatrix(x: 2, y: 2, z: 2)
        let xs = intersect(s, ray: ray)
        
        XCTAssertEqual(gRay.origin == newRGPoint(x: 0, y: 0, z: -2.5) , true)
        XCTAssertEqual(gRay.direction == newRGVector(x: 0, y: 0, z: 0.5) , true)
    }
    
    func testIntersectingAfterRefactorTranslate() {
        // Intersecting a translated shape with a ray
        // Given
        let ray = newRGRay(origin: newRGPoint(x: 0, y: 0, z: -5), direction: newRGVector(x: 0, y: 0, z: 1))
        var s = RGSphere()
        
        // When
        s.transform = newRGTranslationMatrix(x: 5, y: 0, z: 0)
        let xs = intersect(s, ray: ray)
        
        // Then
        XCTAssertEqual(gRay.origin == newRGPoint(x: -5, y: 0, z: -5) , true)
        XCTAssertEqual(gRay.direction == newRGVector(x: 0, y: 0, z: 1) , true)
    }
    
    func testNormalOnTranslatedShape() {
        // Given
        var s = RGSphere()
        
        // When
        s.transform = newRGTranslationMatrix(x: 0, y: 1, z: 0)
        let n = normalAt(s, newRGPoint(x: 0, y: 1.70711, z: -0.70711))
        
        // Then
        XCTAssertEqual(equalTuples(n, newRGVector(x: 0, y: 0.70711, z: -0.70711)), true)
    }
    
    func testNormalOnTransformedShape() {
        // Given
        var s = RGSphere()
        let m = newRGScalingMatrix(x: 1, y: 0.5, z: 1) * newRGRotationZ(Double.pi/5.0)
        
        // When
        s.transform = m
        let n = normalAt(s, newRGPoint(x: 0, y: sqrt(2.0)/2.0, z: -(sqrt(2.0)/2.0)))
        
        let result = newRGVector(x: 0, y: 0.97014, z: -0.24254)
        
        n.printTuple()
        result.printTuple()
        // Then
        XCTAssertEqual(equalTuples(n, result), true)
    }
    
    func testPlane() {
        // Given
        var p = RGPlane()
        
        // When
        let n1 = p.localNormalAt(point: newRGPoint(x:  0, y: 0, z:   0 ))
        let n2 = p.localNormalAt(point: newRGPoint(x: 10, y: 0, z: -10 ))
        let n3 = p.localNormalAt(point: newRGPoint(x: -5, y: 0, z: 150 ))
        
        // Then
        XCTAssertEqual(n1 == newRGVector(x: 0, y: 1, z: 0), true)
        XCTAssertEqual(n2 == newRGVector(x: 0, y: 1, z: 0), true)
        XCTAssertEqual(n3 == newRGVector(x: 0, y: 1, z: 0), true)
    }
    
    func testIntersectingPlane() {
        // Given
        let p = RGPlane()
        // and
        let r = newRGRay(origin: newRGPoint(x: 0, y: 10, z: 0), direction: newRGVector(x: 0, y: 0, z: 1))
        
        // When
        let xs = p.localIntersect(tRay: r)
        
        // Then
        XCTAssertEqual(xs.isEmpty, true)
        
    }
    
    func testIntersectingPlaneFromAbove() {
        // Given
        let p = RGPlane()
        let r = newRGRay(origin: newRGPoint(x: 0, y: 1, z: 0), direction: newRGVector(x: 0, y: -1, z: 0))
        
        // When
        let xs = p.localIntersect(tRay: r)
        
        // Then
        XCTAssertEqual(xs.count == 1, true)
        XCTAssertEqual(xs[0].t == 1, true)
        XCTAssertEqual(xs[0].object.id == p.id, true)
    }
    
    func testIntersectingPlaneFromBelow() {
        // Given
        let p = RGPlane()
        let r = newRGRay(origin: newRGPoint(x: 0, y: -1, z: 0), direction: newRGVector(x: 0, y: 1, z: 0))
        
        // When
        let xs = p.localIntersect(tRay: r)
        
        // Then
        XCTAssertEqual(xs.count == 1, true)
        XCTAssertEqual(xs[0].t == 1, true)
        XCTAssertEqual(xs[0].object.id == p.id, true)
    }
}
