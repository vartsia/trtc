//
//  RGRender.swift
//  RGCore
//
//  Created by Juhani Nikumaa on 20/08/2019.
//
//  MIT License
//  Copyright (c) 2019 Juhani Nikumaa
//  vartsia.net
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import Foundation
import simd

public func renderRGWorldWithCamera(w: RGWorld, c: RGCamera) -> RGCanvas {
    var canvas = RGCanvas(size: RGSize(width: c.hSize, height: c.vSize))
    
    for y in 0..<c.vSize {
        for x in 0..<c.hSize {
            let r = newRGRayForPixel(c: c, x: x, y: y)
            let color = RGColorAt(world: w, ray: r)
            _ = writePixelToRGCanvas(&canvas, position: RGPosition(x: x.toUInt32, y: y.toUInt32), color: color)
        }
    }
    
    return canvas
}
