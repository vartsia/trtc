//
// RGIntersection.swift
// RGCore
//
// Created by Juhani Nikumaa on 12/05/2019
//
// MIT License
// Copyright (c) 2019 Juhani Nikumaa
// vartsia.net
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.


import Foundation
import simd


/**
 Stores infromation of an object that was intersected at given location t.
 */
public struct RGIntersection {
    public var t: Double
    public var object: RGSphere
    
    public init(t: Double, object: RGSphere) {
        self.t = t
        self.object = object
    }
}


/**
 Generates and array of RGIntersections.
 Variable number of intersections could be given to function.
 Ex. let i = intersectinonsRG(i: inters1, inters2, inters3)
 */
public func intersectinonsRG(i: RGIntersection...) -> [RGIntersection] {
    var intersections: [RGIntersection] = []
    
    for intersection in i {
        intersections.append(intersection)
    }
    
    // Sort these here so the hits function can find the hit faster.
    return intersections.sorted(by: {
        $0.t < $1.t
    })
}


/**
 Hit will be the first intersection where t is positive.
 If t is negative the intersection is behind the origin.
 */
public func hitRG(intersections: [RGIntersection]) -> RGIntersection? {
    for i in intersections {
        if i.t > 0 {
            return i
        }
    }
    return nil
}


/**
 Function that is used to sort the intersections.
 Could be pased to arrays sorted function is it contains RGIntersection objects.
 */
func sortIntersection(l: RGIntersection, r: RGIntersection) -> Bool {
    return l.t < r.t
}


/**
 Calculates the itersections points for a ray to a sphere.
 - Returns: Array of RGIntersections.
 */
public func intersectRGSphere(_ sphere: RGSphere, ray: RGRay) -> [RGIntersection] {
    var t: [RGIntersection] = []
    
    let tRay = transformRGRay(ray, matrix: simd_inverse(sphere.transform))
    
    /*
     The vector form the sphere's center, to the ray origin
     # Remember: the sphere is centered at the world origin
     */
    let p = newRGPoint(x: 0, y: 0, z: 0)
    
    let shpere_to_ray = tRay.origin - p
    
    let a = simd_dot(tRay.direction, tRay.direction)
    
    let b = Double(2.0) * simd_dot(tRay.direction, shpere_to_ray)
    
    let c = simd_dot(shpere_to_ray, shpere_to_ray) - Double(1.0)
    
    let discriminant = pow(Double(b), 2.0) - (Double(4.0) * a * c)
    
    if discriminant < Double(0.0) {
        return t
    }
    
    let t1 = (-b - sqrt(discriminant)) /  (Double(2.0) * a)
    let t2 = (-b + sqrt(discriminant)) /  (Double(2.0) * a)
    
    t.append(RGIntersection(t: t1, object: sphere))
    t.append(RGIntersection(t: t2, object: sphere))
    
    return t
}


/**
 Finds all the intersections in a given worlds objects for a given ray.
 - Returns: Array of RGIntersections.
 */
public func intersectRGWorld(world: RGWorld, ray: RGRay) -> [RGIntersection] {
    var intersections: [RGIntersection] = []
    
    for sphere in world.objects {
       intersections.append(contentsOf: intersectRGSphere(sphere, ray: ray))
    }
    
    let sorted = intersections.sorted(by: sortIntersection)
    return sorted
}
