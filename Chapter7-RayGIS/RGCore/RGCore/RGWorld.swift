//
// RGWorld.swift
// RGCore
//
// Created by Juhani Nikumaa on 03/08/2019
//
// MIT License
// Copyright (c) 2019 Juhani Nikumaa
// vartsia.net
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.


import Foundation


/**
 RGWorld hold the object inside the world and the lightsoucre for the world.
 In this point there is only one lightsource.
 */
public struct RGWorld {
    public var lightSource: RGPointLight?
    public var objects: [RGSphere] = []
    
    public init() {
        
    }
}

/**
 Constructs a default world that have thwo spheres and lightsource.
 These are needed for tests in Chapter 7.
 */
public func defaultRGWorld() -> RGWorld {
    var world = RGWorld()
    
    var s1 = RGSphere()
    s1.material.color = newRGColor(red: 0.8, green: 1.0, blue: 0.6)
    s1.material.diffuse = 0.7
    s1.material.specular = 0.2
    
    var s2 = RGSphere()
    s2.transform = newRGScalingMatrix(x: 0.5, y: 0.5, z: 0.5)
    
    world.lightSource = RGPointLight(intensity: RGColor(1.0, 1.0, 1.0),
                                     location: newRGPoint(x: -10.0, y: -10.0, z: -10.0))
    world.objects.append(s1)
    world.objects.append(s2)
    
    return world
}
