//
// RGPointLight.swift
// RGCore
//
// Created by Juhani Nikumaa on 02/06/2019
//
// MIT License
// Copyright (c) 2019 Juhani Nikumaa
// vartsia.net
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.


import Foundation
import simd


/**
 Point light that have its position and intensity.
 Equatable implementation is added for Chapter 7.
 */
public struct RGPointLight: Equatable {
    public var intensity: RGColor
    public var location: RGTuple
    
    
    /**
     Implements the Equatable protocol.
     */
    public static func == (l: RGPointLight, r: RGPointLight) -> Bool {
        return
            equalColors(l.intensity, r.intensity) &&
            equalTuples(l.location, r.location)
        
    }
}


/**
 Convinience function to create new RGPointLight.
 */
public func newRGPointLight(_ intensity: RGColor, _ location: RGTuple) -> RGPointLight {
    return RGPointLight(intensity: intensity, location: location)
}
