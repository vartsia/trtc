//
//  RGCamera.swift
//  RGCore
//
//  Created by Juhani Nikumaa on 18/08/2019.
//
//  MIT License
//  Copyright (c) 2019 Juhani Nikumaa
//  vartsia.net
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import Foundation


public class RGCamera {
    public var hSize: Int  {
        didSet {
            calcPixelSize()
        }
    }
    public var vSize: Int  {
        didSet {
            calcPixelSize()
        }
    }
    public var fOfView: Double {
        didSet {
            calcPixelSize()
        }
    }
    public var transformation: RGMatrix4x4
    public private(set) var halfWidth: Double = 0
    public private(set) var halfHeight: Double = 0
    public private(set) var pixelSize: Double = 1.0
    
    private func calcPixelSize() {
        let halfView = tan(fOfView / 2.0)
        let aspectRatio = hSize.d / vSize.d
        if aspectRatio >= 1 {
            halfWidth = halfView
            halfHeight = halfView / aspectRatio
        } else {
            halfWidth = halfView * aspectRatio
            halfHeight = halfView
        }
        
        pixelSize = (halfWidth * 2.0) / hSize.d
    }
    
    public init(hsize: Int, vsize: Int, fieldOfView: Double) {
        hSize = hsize
        vSize = vsize
        fOfView = fieldOfView
        transformation = RGMatrix4x4.identity()
        calcPixelSize()
    }
}
