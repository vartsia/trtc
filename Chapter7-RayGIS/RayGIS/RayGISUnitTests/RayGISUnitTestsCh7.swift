//
// RayGISUnitTestsCh7.swift
// RayGISUnitTests
//
// Created by Juhani Nikumaa on 02/07/2019
//
// MIT License
// Copyright (c) 2019 Juhani Nikumaa
// vartsia.net
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.


import XCTest
import RGCore
import simd

class RayGISUnitTestsCh7: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testCreateWorld() {
        // Given
        let w = RGWorld()
        
        // Then
        XCTAssertEqual(w.objects.isEmpty, true)
    }
    
    func testDefaultWorld() {
        // Given
        let w = defaultRGWorld()
        
        // When
        let pointLight = newRGPointLight(newRGColor(red: 1, green: 1, blue: 1), newRGPoint(x: -10, y: -10, z: -10))
        let color = newRGColor(red: 0.8, green: 1.0, blue: 0.6)
        let diffuse = 0.7
        let specular = 0.2
        let scaling = newRGScalingMatrix(x: 0.5, y: 0.5, z: 0.5)
        
        var s1 = RGSphere()
        s1.material.color = color
        s1.material.diffuse = diffuse
        s1.material.specular = specular
        
        var s2 = RGSphere()
        s2.transform = scaling
        
        // Then
        XCTAssertEqual(w.lightSource! == pointLight, true)
        XCTAssertEqual(w.objects.contains(where: { $0 == s1 }), true)
        XCTAssertEqual(w.objects.contains(where: { $0 == s2 }), true)
    }

    func testDefaultWorldIntersection() {
        // Given
        let w = defaultRGWorld()
        let r = newRGRay(origin: newRGPoint(x: 0, y: 0, z: -5.0), direction: newRGVector(x: 0, y: 0, z: 1))
        
        // When
        let xs = intersectRGWorld(world: w, ray: r)
        
        // Then
        XCTAssertEqual(xs.count == 4, true)
        XCTAssertEqual(xs[0].t == 4.0, true)
        XCTAssertEqual(xs[1].t == 4.5, true)
        XCTAssertEqual(xs[2].t == 5.5, true)
        XCTAssertEqual(xs[3].t == 6.0, true)
    }
    
    func testPrepareComputations() {
        // Given
        let r = newRGRay(origin: newRGPoint(x: 0, y: 0, z: -5), direction: newRGVector(x: 0, y: 0, z: 1))
        let shape = RGSphere()
        let i = RGIntersection(t: 4, object: shape)
        
        // When
        let comps = prepareRGComputation(intersection: i, ray: r)
        
        // Then
        XCTAssertEqual(comps.t.isEqualTo(i.t), true)
        XCTAssertEqual(comps.object == i.object, true)
        XCTAssertEqual(comps.point == newRGPoint(x: 0, y: 0, z: -1), true)
        XCTAssertEqual(comps.eyev == newRGVector(x: 0, y: 0, z: -1), true)
        XCTAssertEqual(comps.normal == newRGVector(x: 0, y: 0 , z: -1), true)
    }
    
    func testIntersectionsOnOutside() {
        // Scenario #1
        // The hit, when an intersection occurs on the outside
        // Given
        let ray = newRGRay(origin: newRGPoint(x: 0, y: 0, z: -5), direction: newRGVector(x: 0, y: 0, z: 1))
        let shape = RGSphere()
        let intersection = RGIntersection(t: 4, object: shape)
        
        // When
        let comps = prepareRGComputation(intersection: intersection, ray: ray)
        
        // Then
        XCTAssertEqual(comps.inside, false)
        
        // Scenario #2
        // The hit, when an intersection occurs on the inside
        // Given
        let ray2 = newRGRay(origin: newRGPoint(x: 0, y: 0, z: 0), direction: newRGVector(x: 0, y: 0, z: 1))
        let shape2 = RGSphere()
        let intersection2 = RGIntersection(t: 1, object: shape2)
        
        // When
        let comps2 = prepareRGComputation(intersection: intersection2, ray: ray2)
        
        // Then
        XCTAssertEqual(comps2.point == newRGPoint(x: 0, y: 0, z: 1 ), true)
        XCTAssertEqual(comps2.eyev == newRGVector(x: 0, y: 0, z: -1), true)
        XCTAssertEqual(comps2.inside, true)
        XCTAssertEqual(comps2.normal == newRGVector(x: 0, y: 0, z: -1), true)
    }
    
    func testShadingAnIntersection() {
        // Given
        let w = defaultRGWorld()
        let r = newRGRay(origin: newRGPoint(x: 0, y: 0, z: -5), direction: newRGVector(x: 0, y: 0, z: 1))
        let s = w.objects.first!
        let i = RGIntersection(t: 4, object: s)
        
        // When
        let comps = prepareRGComputation(intersection: i, ray: r)
        let c = newRGShadeHit(world: w, computations: comps)
        print(c)
        // Then
        XCTAssertEqual(c == newRGColor(red: 0.38066, green: 0.47583, blue: 0.2855), true)
    }
    
    func testShadingAnIntersectionFromInside() {
        // Given
        var w = defaultRGWorld()
        w.lightSource = newRGPointLight(RGColor(x: 1, y: 1, z: 1), newRGPoint(x: 0, y: 0.25, z: 0))
        let r = newRGRay(origin: newRGPoint(x: 0, y: 0, z: 0), direction: newRGVector(x: 0, y: 0, z: 1))
        let s = w.objects[1]
        let i = RGIntersection(t: 0.5, object: s)
        
        // When
        let comps = prepareRGComputation(intersection: i, ray: r)
        let c = newRGShadeHit(world: w, computations: comps)
        print(c)
        // Then
        XCTAssertEqual(c == newRGColor(red: 0.90498, green: 0.90498, blue: 0.90498), true)
    }
    
    
    func testColorAtWhenRayMisses() {
        // Given
        let w = defaultRGWorld()
        let r = newRGRay(origin: newRGPoint(x: 0, y: 0, z: -5), direction: newRGVector(x: 0, y: 1, z: 0))
        
        // When
        let c = RGColorAt(world: w, ray: r)
        
        // Then
        XCTAssertEqual(c == newRGColor(red: 0, green: 0, blue: 0), true)
    }

    func testColorAtWhenRayHits() {
        // Given
        let w = defaultRGWorld()
        let r = newRGRay(origin: newRGPoint(x: 0, y: 0, z: -5), direction: newRGVector(x: 0, y: 0, z: 1))
        
        // When
        let c = RGColorAt(world: w, ray: r)
        
        // Then
        XCTAssertEqual(c == newRGColor(red: 0.38066, green: 0.47583, blue: 0.2855), true)
    }
    
    func testDefaultViewTransformation() {
        // Given
        let from = newRGPoint(x: 0, y: 0, z: 0)
        let to = newRGPoint(x: 0, y: 0, z: -1)
        let up = newRGVector(x: 0, y: 1, z: 0)
        
        // When
        let t = newRGViewTransformation(from, to: to, up: up)
        // Then
        XCTAssertEqual(t == RGMatrix4x4.identity(), true)
    }
    
    func testViewTransformationLookingPositiveZ() {
        // Given
        let from = newRGPoint(x: 0, y: 0, z: 0)
        let to = newRGPoint(x: 0, y: 0, z: 1)
        let up = newRGVector(x: 0, y: 1, z: 0)
        
        // When
        let t = newRGViewTransformation(from, to: to, up: up)
        // Then
        XCTAssertEqual(t == newRGScalingMatrix(x: -1, y: 1, z: -1), true)
    }
    
    func testViewTransformationMovesTheWorld() {
        // Given
        let from = newRGPoint(x: 0, y: 0, z: 8)
        let to = newRGPoint(x: 0, y: 0, z: 0)
        let up = newRGVector(x: 0, y: 1, z: 0)
        
        // When
        let t = newRGViewTransformation(from, to: to, up: up)
        // Then
        XCTAssertEqual(t == newRGTranslationMatrix(x: 0, y: 0, z: -8), true)
    }
    
    func testViewTransformationArbitrary() {
        // Given
        let from = newRGPoint(x: 1, y: 3, z: 2)
        let to = newRGPoint(x: 4, y: -2, z: 8)
        let up = newRGVector(x: 1, y: 1, z: 0)
        
        // When
        let t = newRGViewTransformation(from, to: to, up: up)
        print(t)
        // Then
        var test =  RGMatrix4x4.identity()
        test[0].x = -0.50709
        test[0].y =  0.76772
        test[0].z = -0.35857
        test[0].w =  0.00000
    
        test[1].x = 0.50709
        test[1].y = 0.60609
        test[1].z = 0.59761
        test[1].w = 0.00000
            
        test[2].x =  0.67612
        test[2].y =  0.12122
        test[2].z = -0.71714
        test[2].w =  0.00000
            
        test[3].x = -2.36643
        test[3].y = -2.82843
        test[3].z =  0.00000
        test[3].w =  1.00000
            
            
        XCTAssertEqual(t.isEqualTo(test), true)
    }
    
    func testConstructingCamera() {
        // Given
        let hsize = 160
        let vsize = 120
        let fieldOfView = Double.pi / 2.0
        
        // When
        let c = RGCamera(hsize: hsize, vsize: vsize, fieldOfView: fieldOfView)
        
        // Then
        XCTAssertEqual(c.hSize == 160, true)
        XCTAssertEqual(c.vSize == 120, true)
        XCTAssertEqual(c.fOfView == Double.pi/2.0, true)
        XCTAssertEqual(c.transformation.isEqualTo(RGMatrix4x4.identity()), true)
    }
    
    /**
     cH is the horizontal canvas
     cV is for vertical canvas
     */
    func testPixelSizeForHorizontCanvas() {
        // Given
        let cH = RGCamera(hsize: 200, vsize: 125, fieldOfView: Double.pi / 2.0)
        
        // Then
        XCTAssertEqual(cH.pixelSize.isEqualTo(0.01), true)
        
        // Given
        let cV = RGCamera(hsize: 125, vsize: 200, fieldOfView: Double.pi / 2.0)
        
        // Then
        XCTAssertEqual(cV.pixelSize.isEqualTo(0.01), true)
        
    }
    
    func testConstructingRayTroughCenterOfCanvas() {
        // Given
        let c = RGCamera(hsize: 201, vsize: 101, fieldOfView: Double.pi/2.0)
        
        // When
        let ray = newRGRayForPixel(c: c, x: 100, y: 50)
        // Then
        XCTAssertEqual(ray.origin.isEqualTo(newRGPoint(x: 0, y: 0, z: 0)), true)
        XCTAssertEqual(ray.direction.isEqualTo(newRGVector(x: 0, y: 0, z: -1)), true)
    }
    
    func testConstructingRayTroughCornerOfCanvas() {
        // Given
        let c = RGCamera(hsize: 201, vsize: 101, fieldOfView: Double.pi/2.0)
        
        // When
        let ray = newRGRayForPixel(c: c, x: 0, y: 0)
        // Then
        XCTAssertEqual(ray.origin.isEqualTo(newRGPoint(x: 0, y: 0, z: 0)), true)
        XCTAssertEqual(ray.direction.isEqualTo(newRGVector(x: 0.66519, y: 0.33259, z: -0.66851)), true)
    }

    func testConstructingRayWhenCameraIsTransformed() {
        // Given
        var c = RGCamera(hsize: 201, vsize: 101, fieldOfView: Double.pi/2.0)
        
        // When
        c.transformation = newRGRotationY(Double.pi / 4.0) * newRGTranslationMatrix(x: 0, y: -2, z: 5)
        let ray = newRGRayForPixel(c: c, x: 100, y: 50)
        // Then
        XCTAssertEqual(ray.origin.isEqualTo(newRGPoint(x: 0, y: 2, z: -5)), true)
        XCTAssertEqual(ray.direction.isEqualTo(newRGVector(x: sqrt(2.0)/2.0, y: 0, z: -sqrt(2.0)/2.0)), true)
    }
    
    func testRenderingWorldWithCamera() {
        // Given
        let world = defaultRGWorld()
        let c = RGCamera(hsize: 11, vsize: 11, fieldOfView: Double.pi/2.0)
        let from = newRGPoint(x: 0, y: 0, z: -5)
        let to = newRGPoint(x: 0, y: 0, z: 0)
        let up = newRGVector(x: 0, y: 1, z: 0)
        c.transformation = newRGViewTransformation(from, to: to, up: up)
        
        // When
        let image = renderRGWorldWithCamera(w: world, c: c)
        
        // Then
        XCTAssertEqual(image.pixelAt(RGPosition(x: 5, y: 5)) == newRGColor(red: 0.38066, green: 0.47583, blue: 0.2855), true)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
