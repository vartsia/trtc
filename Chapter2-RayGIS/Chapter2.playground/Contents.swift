//
//  MIT License
//  Copyright (c) 2019 Juhani Nikumaa
//  vartsia.net
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.


import Cocoa
import RGCore
import simd


/**
 The Ray Tracing Challenge
 Chapter 2 - Putting it together challend at the end of the chapter.
 Projectile to canvas and save to ppm file.
 */


struct Projectile {
    var position: RGTuple
    var velocity: RGTuple
}


struct Environment {
    var gravity: RGTuple
    var wind: RGTuple
}


/**
 This could be called in a loop.
 Returns new Projectile based on the previous and the cahange is based on the environment settings.
 */
func tick(environment: Environment, projectile: Projectile) -> Projectile {
    let pos = projectile.position + projectile.velocity
    let vel = projectile.velocity + environment.gravity + environment.wind
    
    return Projectile(position: pos, velocity: vel)
}


/**
 Chapter 2.
 The changes for the Chapter 2 starts here.
 First we initialize the p and e with values Jamis mentions in the book
 */


/**
 Projectile and Environment parameters are set baed on the books recommendations.
 */
var p = Projectile(position: newRGPoint(x: 0, y: 1, z: 0), velocity: newRGVector(x: 1, y: 1.8, z: 0).normalize() * 11.25 )
var e = Environment(gravity: newRGVector(x: 0, y: -0.1, z: 0), wind: newRGVector(x: -0.01, y: 0, z: 0))



/**
 Secon thing is to create the canvas.
 Canvas size is based on the books succestion.
 */
var c = newRGCanvas(width: 900, height: 500)

/**
 I choose to use green as the projectile color.
 */
var clr = newRGColor(red: 0, green: 1.0, blue: 0)

/**
 Loop to generate the projectiles positions though the sapce.
 */
while p.position.y > 0.0 {
    p = tick(environment: e, projectile: p)
    

    /**
     Remember to subtract the y from canvas height.
     ppm coordinate system is upside down comapred to world coorinates.
     */
    let pos = p.position.getRGPosition()
    let pixelPosition = RGPosition(pos.x, c.size.height.toUInt32 - pos.y)
    
    /**
     This is here just so we can visualize the y value on playground.
     */
    _ = pos.y
    
    /**
     Print the values so its easier to see what actually happens under cover.
     */
    print(pixelPosition.description)
    
    /**
     When we got the positon we write it into canvas.
     */
    c.writePixelAt(pixelPosition, color: clr)
}

/**
 Generates the ppm ASCII file string.
 */
let ppmFile = canvasToPPMString(c)

/**
 Write the projectile image on a file in desktop.
 */
let home = URL(fileURLWithPath: NSHomeDirectory())
let url = home.appendingPathComponent("Desktop/TRTC_image3.ppm")
print(url)
do {
    try ppmFile.write(to: url, atomically: false, encoding: .utf8)
} catch {
    print(error)
}
