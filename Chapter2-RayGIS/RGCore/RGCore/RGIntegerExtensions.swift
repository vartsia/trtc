//
//  RGIntegerpublic extensions.swift
//  RGCore
//
//  Created by Juhani Nikumaa on 28/03/2019.
//
//  MIT License
//  Copyright (c) 2019 Juhani Nikumaa
//  vartsia.net
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.


import Foundation


public extension Int {
    var toUInt64: UInt64 {
        get {
            return UInt64(self)
        }
    }
}


public extension Int {
    var toUInt32: UInt32 {
        get {
            return UInt32(self)
        }
    }
}


public extension UInt64 {
    var toInt: Int {
        get {
            return Int(self)
        }
    }
}


public extension UInt64 {
    var toUInt32: UInt32 {
        get {
            return UInt32(self)
        }
    }
}


public extension UInt32 {
    var toInt: Int {
        get {
            return Int(self)
        }
    }
}
