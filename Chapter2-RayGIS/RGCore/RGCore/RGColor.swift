//
//  RGColor.swift
//  RGCore
//
//  Created by Juhani Nikumaa on 27/03/2019.
//
//  MIT License
//  Copyright (c) 2019 Juhani Nikumaa
//  vartsia.net
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import Foundation
import simd

/**
 Chapter 2.
 */
public typealias RGColor = simd_float3


/**
 RGColor extension to get inner values using typical channel names:
 red, green and blue.
 */
public extension RGColor {
    var red: Float {
        get {
            return self.x
        }
        set {
            self.x = newValue
        }
    }
    
    
    var green: Float {
        get {
            return self.y
        } set {
            self.y = newValue
        }
    }
    
    
    var blue: Float {
        get {
            return self.z
        }
        set {
            self.z = newValue
        }
    }
    
    var R: Int {
        get {
            let r = self.red
            if r < 0.0 {
                return 0
                }
            else if r > 1.0 {
                return 255
                }
            else {
                return Int(r * Float(255).rounded())
                }
        }
    }
    
    
    var G: Int {
        get {
            let g = self.green
            if g < 0.0 {
                return 0
            }
            else if g > 1.0 {
                return 255
            }
            else {
                return Int(g * Float(255).rounded() )
            }
        }
    }
    
    
    var B: Int {
        get {
            let b = self.blue
            if b < 0.0 {
                return 0
            }
            else if b > 1.0 {
                return 255
            }
            else {
                return Int(b * Float(255).rounded())
            }
        }
    } 
}

/**
 Creates new color without arguments.
 Initializes all values to 0.0
 */

public func newRGColorBlack() -> RGColor {
    return newRGColor(red: 0.0, green: 0.0, blue: 0.0)
}

/**
 Factory function to create a new RGColor.
 */
public func newRGColor(red: Float, green: Float, blue: Float) -> RGColor {
    return RGColor(x: red, y: green, z: blue)
}


/**
 RGColor equality function that uses Float extension isEqualTo
 which is based on the epsilon comparison.
 */
public func equalColors(_ lT: RGColor, _ rT: RGColor) -> Bool {
    if  lT.x.isEqualTo(rT.x) &&
        lT.y.isEqualTo(rT.y) &&
        lT.z.isEqualTo(rT.z) {
        return true
        }
    else {
        return false
        }
}
