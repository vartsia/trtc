//
//  RayGISUnitTestsDeprecated.swift
//  RayGISUnitTestsDeprecated
//
//  Created by Juhani Nikumaa on 23/01/2019.
//
//  MIT License
//  Copyright (c) 2019 Juhani Nikumaa
//  vartsia.net
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

//  TESTS for RGCore.


import XCTest

import AppKit

@testable import RGCore

class RayGISUnitTestsDeprecated: XCTestCase {

    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }


    /**
     Test tuple creation.
     This combines the first two test cases.
     XCTest test method must begin with test...
     */
    func testRGTuplesCreation() {
        // Given
        let aX: Float =  4.3
        let aY: Float = -4.2
        let aZ: Float =  3.1
        // When
        let point = RGTupleDeprecated(x: aX, y: aY, z: aZ, w: 1.0)
        let vector = RGTupleDeprecated(x: aX, y: aY, z: aZ, w: 0.0)
        // Then
        
        XCTAssertEqual(point.x,  4.3)
        XCTAssertEqual(point.y, -4.2)
        XCTAssertEqual(point.z,  3.1)
        XCTAssertEqual(point.w,  1.0) // 1.0 when point
        
        XCTAssertEqual(vector.x,  4.3)
        XCTAssertEqual(vector.y, -4.2)
        XCTAssertEqual(vector.z,  3.1)
        XCTAssertEqual(vector.w,  0.0) // 0.0 when vector
        
        XCTAssertEqual(isPoint(point), true)
        XCTAssertEqual(isVector(vector), true)
    }
    
    
    func testRGTuplesEquality() {
        // Equality for points.
        // Given
        let aX: Float =  4.3
        let aY: Float = -4.2
        let aZ: Float =  3.1
        
        // When
        // Point is aPoint
        let aPoint = newPoint(x: aX, y: aY, z: aZ)
        // and tupels to test against.
        let tuple1a = RGTupleDeprecated(x: 4.3, y: -4.2, z: 3.1, w: 1)
        let tuple2a = RGTupleDeprecated(x: 4.33, y: -4.20, z: 3.10, w: 1)
        
        // Then
        XCTAssertEqual(aPoint.isEqualTo(tuple1a), true)  // equal
        XCTAssertEqual(aPoint.isEqualTo(tuple2a), false) // not equal
        
        // and When Vector
        let a1Vector = newVector(x: aX, y: aY, z: aZ)
        let tuple1b = RGTupleDeprecated(x: 4.3, y: -4.2, z: 3.1, w: 0)
        let tuple2b = RGTupleDeprecated(x: 4.33, y: -4.20, z: 3.10, w: 0)
        
        XCTAssertEqual(a1Vector.isEqualTo(tuple1b), true)    // equal
        XCTAssertEqual(a1Vector.isEqualTo(tuple2b), false)   // not equal
    }
    
    
    /**
     Test case for testing tuples adding.
     */
    func testRGTuplesAdd() {
        // Given
        let pX: Float =  3.0
        let pY: Float = -2.0
        let pZ: Float =  5.0
        
        let vX: Float = -2.0
        let vY: Float =  3.0
        let vZ: Float =  1.0
        
        // When
        let point = newPoint(x: pX, y: pY, z: pZ)
        let vector = newVector(x: vX, y: vY, z: vZ)
        let pointNew = point + vector
        let vToPresult = RGTupleDeprecated(x: 1.0, y: 1.0, z: 6.0, w: 1.0)
        
        let vectorNew = vector + vector
        let vToVresult = RGTupleDeprecated(x: -4.0, y: 6.0, z: 2.0, w: 0.0)
        // Then
        XCTAssertEqual(pointNew.isEqualTo(vToPresult), true)
        XCTAssertEqual(vectorNew.isEqualTo(vToVresult), true)
    }
    
    /**
     Testing tuples subtract.
     */
    func testRGTuplesSubtract() {
        // Given
        let a1X: Float = 3
        let a1Y: Float = 2
        let a1Z: Float = 1
        
        let a2X: Float = 5
        let a2Y: Float = 6
        let a2Z: Float = 7
        
        // When subtract two points -> result is vector.
        let a1P = newPoint(x: a1X, y: a1Y, z: a1Z)
        let a2P = newPoint(x: a2X, y: a2Y, z: a2Z)
        let aResult = a1P - a2P
        let aCompare = RGTupleDeprecated(x: -2, y: -4, z: -6, w: 0)
        
        // Then
        XCTAssert(aResult.isEqualTo(aCompare), "result: \(aResult) aCompareTo: \(aCompare)")
        XCTAssertEqual(aResult.isEqualTo(aCompare), true)
        
        // When subtract vector from point -> result is point.
        let b1P = newPoint(x: a1X, y: a1Y, z: a1Z)
        let b2V = newVector(x: a2X, y: a2Y, z: a2Z)
        let bResult = b1P - b2V
        let bCompare = RGTupleDeprecated(x: -2, y: -4, z: -6, w: 1)
        
        // Then
        XCTAssertEqual(bResult.isEqualTo(bCompare), true)
        
        // When subtract two vectors -> result is vector.
        let c1P = newVector(x: a1X, y: a1Y, z: a1Z)
        let c2V = newVector(x: a2X, y: a2Y, z: a2Z)
        let cResult = c1P - c2V
        let cCompare = RGTupleDeprecated(x: -2, y: -4, z: -6, w: 0)
        
        // Then
        XCTAssertEqual(cResult.isEqualTo(cCompare), true)
        
    }
    
    
    func testNegate() {
        // Given
        let x: Float =  1
        let y: Float = -2
        let z: Float =  3
        let w: Float = -4
        
        // When
        let cmpr = RGTupleDeprecated(x: -1, y: 2, z: -3, w: 4)
        let t1 = -RGTupleDeprecated(x: x, y: y, z: z, w: w)
        var t2 = RGTupleDeprecated(x: x, y: y, z: z, w: w)
        t2.negate()
        
        // Then
        XCTAssertEqual(cmpr.isEqualTo(t1), true)
        XCTAssertEqual(cmpr.isEqualTo(t2), true)
    }
    
    
    func testMultiply() {
        // Given
        let x: Float =  1
        let y: Float = -2
        let z: Float =  3
        let w: Float = -4
        
        let sA: Float = 3.5 // First scalar to multiply tuple.
        let sB: Float = 0.5 // Second scalar to multiply tuple.
        
        // When
        let cmprA = RGTupleDeprecated(x: 3.5, y: -7, z: 10.5, w: -14)
        let cmprB = RGTupleDeprecated(x: 0.5, y: -1, z: 1.5, w: -2)
        
        let base = RGTupleDeprecated(x: x, y: y, z: z, w: w)
        let aMultip = sA * base
        let bMultip = sB * base
        
        // Then
        XCTAssertEqual(aMultip.isEqualTo(cmprA), true)
        XCTAssertEqual(bMultip.isEqualTo(cmprB), true)
    }
    
    
    func testDivision() {
        // Given
        let x: Float =  1
        let y: Float = -2
        let z: Float =  3
        let w: Float = -4
        
        // When
        let cmpr = RGTupleDeprecated(x: 0.5, y: -1, z: 1.5, w: -2)
        let base = RGTupleDeprecated(x: x, y: y, z: z, w: w)
        
        let div = base / 2
        
        // Then
        XCTAssertEqual(div.isEqualTo(cmpr), true)
        
    }
    
    /**
     Testing the magnitude aka. length of the vector.
     */
    func testMagnitude() {
        // Given
        
        // When
        let v1 = newVector(x: 1, y: 0, z: 0)
        let v2 = newVector(x: 0, y: 1, z: 0)
        let v3 = newVector(x: 0, y: 0, z: 1)
        
        let v4 = newVector(x: 1, y: 2, z: 3)
        let v5 = newVector(x: -1, y: -2, z: -3)
        
        let magCompA: Float = 1.0
        
        // Vectros v4 and v5 the result of magnitude should be √14
        let magCompB: Float = sqrtf(14.0)
        
        XCTAssertEqual(v1.magnitude().isEqual(to: magCompA), true)
        XCTAssertEqual(v2.magnitude().isEqual(to: magCompA), true)
        XCTAssertEqual(v3.magnitude().isEqual(to: magCompA), true)
        XCTAssertEqual(v4.magnitude().isEqual(to: magCompB), true)
        XCTAssertEqual(v5.magnitude().isEqual(to: magCompB), true)
        
    }
    
    /**
     Testing normalization.
     */
    func testNormalization() {
        // Given
        
        let v1 = newVector(x: 4, y: 0, z: 0)
        let v2 = newVector(x: 1, y: 2, z: 3)
        
        let v1Normal = newVector(x: 1, y: 0, z: 0)
        let v2Normal = newVector(x: 1 / sqrtf(14), y: 2 / sqrtf(14), z: 3 / sqrtf(14))
        
        // Then
        let v1Nor = v1.normalize()
        let v2Nor = v2.normalize()
        
        XCTAssertEqual(v1Nor.isEqualTo(v1Normal), true)
        XCTAssertEqual(v1Nor.magnitude().isEqual(to: 1.0), true)
        
        XCTAssertEqual(v2Nor.isEqualTo(v2Normal), true)
        // print("Magnitude of v2Normal \(v2Normal.magnitude())")
        // print("Magnitude of v2Nor    \(v2Nor.magnitude())")
        XCTAssertEqual(v2Nor.magnitude().isEqualTo(1.0), true)
        XCTAssertEqual(v2Normal.magnitude().isEqualTo(1.0), true)
    }
    
    
    /**
     Dot Product testing.
     Dot procut of two tuples.
     */
    func testDotProdcut() {
        // Given
        let vectorA = newVector(x: 1, y: 2, z: 3)
        let vectorB = newVector(x: 2, y: 3, z: 4)
        
        let result: Float = 20.0
        
        // Then
        let dotP = dot(vectorA, vectorB)
        print(dotP)
        XCTAssertEqual(dotP.isEqualTo(result), true)
    }
    
    
    /**
     Cross Product testing.
     - Returns: RGTuple that is a vector type.
     */
    func testCrossProduct() {
        // Given
        let (xA, yA, zA) = (1.0, 2.0, 3.0) as (Float, Float, Float)
        let (xB, yB, zB) = (2.0, 3.0, 4.0) as (Float, Float, Float)
        
        // When
        let vA = newVector(x: xA, y: yA, z: zA)
        let vB = newVector(x: xB, y: yB, z: zB)
        
        // Then
        let crossAB = cross(vA, vB)
        let crossBA = cross(vB, vA)
        
        XCTAssertEqual(crossAB.isEqualTo(newVector(x: -1, y: 2, z: -1)), true)
        XCTAssertEqual(crossBA.isEqualTo(newVector(x:  1, y:-2, z:  1)), true)
    }
    
    
    /**
     Chapter 2.
     Test Colors.
     Create color tuple.
     */
    func testColorCreation() {
        // Given
        let r: Float = -0.5
        let g: Float =  0.4
        let b: Float =  1.7
        
        // When
        let c = newColor(red: r, green: g, blue: b)

        // Then
        XCTAssertEqual(c.red.isEqual(to:  -0.5), true)
        XCTAssertEqual(c.green.isEqual(to: 0.4), true)
        XCTAssertEqual(c.blue.isEqual(to:  1.7), true)
        
    }
    
    
    /**
     Chapter 2.
     Adding is same as for Points and Vectors.
     */
    func testColorAdding() {
        let c1 = newColor(red: 0.9, green: 0.6, blue: 0.75)
        let c2 = newColor(red: 0.7, green: 0.1, blue: 0.25)
        
        let res = (c1 + c2)
        let test = newColor(red: 1.6, green: 0.7, blue: 1.0)
        print(String(describing: res))
        XCTAssertEqual(res.isEqualTo(test), true)
    }

    
    /**
     Chapter 2.
     Subtracting is same as for Points and Vectors.
     */
    func testColorSubtracting() {
        let c1 = newColor(red: 0.9, green: 0.6, blue: 0.75)
        let c2 = newColor(red: 0.7, green: 0.1, blue: 0.25)
        
        let res = (c1 - c2)
        let test = newColor(red: 0.2, green: 0.5, blue: 0.5)
        print(String(describing: res))
        XCTAssertEqual(res.isEqualTo(test), true)
    }
    
    
    // This is example function not implemented yet.
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
