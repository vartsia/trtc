//
//  RayGISUnitTests.swift
//  RayGISUnitTests
//
//  Created by Juhani Nikumaa on 27/03/2019.
//
//  MIT License
//  Copyright (c) 2019 Juhani Nikumaa
//  vartsia.net
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.


import XCTest

import AppKit
import simd

@testable import RGCore

class RayGISUnitTests: XCTestCase {
    
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    
    /*****************************
     * CHAPTER 1
     *****************************/
    
    
    /**
     Test tuple creation.
     This combines the first two test cases.
     XCTest test method must begin with test...
     */
    func testRGTuplesCreation() {
        // Given
        let aX: Float =  4.3
        let aY: Float = -4.2
        let aZ: Float =  3.1
        // When
        let point = RGTuple(x: aX, y: aY, z: aZ, w: 1.0)
        let vector = RGTuple(x: aX, y: aY, z: aZ, w: 0.0)
        // Then
        
        XCTAssertEqual(point.x,  4.3)
        XCTAssertEqual(point.y, -4.2)
        XCTAssertEqual(point.z,  3.1)
        XCTAssertEqual(point.w,  1.0) // 1.0 when point
        
        XCTAssertEqual(vector.x,  4.3)
        XCTAssertEqual(vector.y, -4.2)
        XCTAssertEqual(vector.z,  3.1)
        XCTAssertEqual(vector.w,  0.0) // 0.0 when vector
        
        XCTAssertEqual(isRGPoint(point), true)
        XCTAssertEqual(isRGVector(vector), true)
    }
    
    
    /**
     Chapter 1
     Test tuples equality.
     */
    func testRGTuplesEquality() {
        // Equality for points.
        // Given
        let aX: Float =  4.3
        let aY: Float = -4.2
        let aZ: Float =  3.1
        
        // When
        // Point is aPoint
        let aPoint = newRGPoint(x: aX, y: aY, z: aZ)
        // and tupels to test against.
        let tuple1a = RGTuple(x: 4.3, y: -4.2, z: 3.1, w: 1)
        let tuple2a = RGTuple(x: 4.33, y: -4.20, z: 3.10, w: 1)
        
        // Then
        XCTAssertEqual(equalTuples(aPoint, tuple1a), true)  // equal
        XCTAssertEqual(equalTuples(aPoint, tuple2a), false) // not equal
        
        // and When Vector
        let a1Vector = newRGVector(x: aX, y: aY, z: aZ)
        let tuple1b = RGTuple(x: 4.3, y: -4.2, z: 3.1, w: 0)
        let tuple2b = RGTuple(x: 4.33, y: -4.20, z: 3.10, w: 0)
        
        XCTAssertEqual(equalTuples(a1Vector, tuple1b), true)    // equal
        XCTAssertEqual(equalTuples(a1Vector, tuple2b), false)   // not equal
    }
    
    
    /**
     Chapter 1
     Test case for testing tuples adding.
     */
    func testRGTuplesAdd() {
        // Given
        let pX: Float =  3.0
        let pY: Float = -2.0
        let pZ: Float =  5.0
        
        let vX: Float = -2.0
        let vY: Float =  3.0
        let vZ: Float =  1.0
        
        // When
        let point = newRGPoint(x: pX, y: pY, z: pZ)
        let vector = newRGVector(x: vX, y: vY, z: vZ)
        let pointNew = point + vector
        let vToPresult = RGTuple(x: 1.0, y: 1.0, z: 6.0, w: 1.0)
        
        let vectorNew = vector + vector
        let vToVresult = RGTuple(x: -4.0, y: 6.0, z: 2.0, w: 0.0)
        
        // Then
        XCTAssertEqual(equalTuples(pointNew, vToPresult), true)
        XCTAssertEqual(equalTuples(vectorNew, vToVresult), true)
    }
    
    
    /**
     Chapter 1
     Testing tuples subtract.
     */
    func testRGTuplesSubtract() {
        // Given
        let a1X: Float = 3
        let a1Y: Float = 2
        let a1Z: Float = 1
        
        let a2X: Float = 5
        let a2Y: Float = 6
        let a2Z: Float = 7
        
        // When subtract two points -> result is vector.
        let a1P = newRGPoint(x: a1X, y: a1Y, z: a1Z)
        let a2P = newRGPoint(x: a2X, y: a2Y, z: a2Z)
        let aResult = a1P - a2P
        let aCompare = RGTuple(x: -2, y: -4, z: -6, w: 0)
        
        // Then
        XCTAssert(equalTuples(aResult, aCompare), "result: \(aResult) aCompareTo: \(aCompare)")
        XCTAssertEqual(equalTuples(aResult, aCompare), true)
        
        // When subtract vector from point -> result is point.
        let b1P = newRGPoint(x: a1X, y: a1Y, z: a1Z)
        let b2V = newRGVector(x: a2X, y: a2Y, z: a2Z)
        let bResult = b1P - b2V
        let bCompare = RGTuple(x: -2, y: -4, z: -6, w: 1)
        
        // Then
        XCTAssertEqual(equalTuples(bResult, bCompare), true)
        
        // When subtract two vectors -> result is vector.
        let c1P = newRGVector(x: a1X, y: a1Y, z: a1Z)
        let c2V = newRGVector(x: a2X, y: a2Y, z: a2Z)
        let cResult = c1P - c2V
        let cCompare = RGTuple(x: -2, y: -4, z: -6, w: 0)
        
        // Then
        XCTAssertEqual(equalTuples(cResult, cCompare), true)
        
    }
    
    
    /**
     Chapter 1
     Test tuple negate.
     */
    func testNegate() {
        // Given
        let x: Float =  1
        let y: Float = -2
        let z: Float =  3
        let w: Float = -4
        
        // When
        let cmpr = RGTuple(x: -1, y: 2, z: -3, w: 4)
        let t1 = -RGTuple(x: x, y: y, z: z, w: w)
        var t2 = RGTuple(x: x, y: y, z: z, w: w)
        t2.negate()
        
        // Then
        XCTAssertEqual(equalTuples(cmpr, t1), true)
        XCTAssertEqual(equalTuples(cmpr, t2), true)
    }
    
    
    /**
     Chapter 1
     Test tuple multiply by another tuple.
     */
    func testMultiply() {
        // Given
        let x: Float =  1
        let y: Float = -2
        let z: Float =  3
        let w: Float = -4
        
        let sA: Float = 3.5 // First scalar to multiply tuple.
        let sB: Float = 0.5 // Second scalar to multiply tuple.
        
        // When
        let cmprA = RGTuple(x: 3.5, y: -7, z: 10.5, w: -14)
        let cmprB = RGTuple(x: 0.5, y: -1, z: 1.5, w: -2)
        
        let base = RGTuple(x: x, y: y, z: z, w: w)
        let aMultip = sA * base
        let bMultip = sB * base
        
        // Then
        XCTAssertEqual(equalTuples(aMultip, cmprA), true)
        XCTAssertEqual(equalTuples(bMultip, cmprB), true)
    }
    
    
    /**
     Chapter 1
     Test tuple division.
     */
    func testDivision() {
        // Given
        let x: Float =  1
        let y: Float = -2
        let z: Float =  3
        let w: Float = -4
        
        // When
        let cmpr = RGTuple(x: 0.5, y: -1, z: 1.5, w: -2)
        let base = RGTuple(x: x, y: y, z: z, w: w)
        
        let div = base / 2
        
        // Then
        XCTAssertEqual(equalTuples(div, cmpr), true)
        
    }
    
    
    /**
     Chapter 1
     Testing the magnitude aka. length of the vector.
     */
    func testMagnitude() {
        // Given
        
        // When
        let v1 = newRGVector(x: 1, y: 0, z: 0)
        let v2 = newRGVector(x: 0, y: 1, z: 0)
        let v3 = newRGVector(x: 0, y: 0, z: 1)
        
        let v4 = newRGVector(x: 1, y: 2, z: 3)
        let v5 = newRGVector(x: -1, y: -2, z: -3)
        
        let magCompA: Float = 1.0
        
        // Vectros v4 and v5 the result of magnitude should be √14
        let magCompB: Float = sqrtf(14.0)
        
        XCTAssertEqual(v1.magnitude().isEqual(to: magCompA), true)
        XCTAssertEqual(v2.magnitude().isEqual(to: magCompA), true)
        XCTAssertEqual(v3.magnitude().isEqual(to: magCompA), true)
        XCTAssertEqual(v4.magnitude().isEqual(to: magCompB), true)
        XCTAssertEqual(v5.magnitude().isEqual(to: magCompB), true)
        
    }
    
    
    /**
     Chapter 1
     Testing normalization.
     */
    func testNormalization() {
        // Given
        
        let v1 = newRGVector(x: 4, y: 0, z: 0)
        let v2 = newRGVector(x: 1, y: 2, z: 3)
        
        let v1Normal = newRGVector(x: 1, y: 0, z: 0)
        let v2Normal = newRGVector(x: 1 / sqrtf(14), y: 2 / sqrtf(14), z: 3 / sqrtf(14))
        
        // Then
        let v1Nor = normalize(v1)
        let v2Nor = normalize(v2)
        
        XCTAssertEqual(equalTuples(v1Nor, v1Normal), true)
        print(v1Nor.magnitude())
        XCTAssertEqual(v1Nor.magnitude().isEqualTo( 1.0), true)
        
        XCTAssertEqual(equalTuples(v2Nor, v2Normal), true)
        // print("Magnitude of v2Normal \(v2Normal.magnitude())")
        // print("Magnitude of v2Nor    \(v2Nor.magnitude())")
        XCTAssertEqual(v2Nor.magnitude().isEqualTo(1.0), true)
        XCTAssertEqual(v2Normal.magnitude().isEqualTo(1.0), true)
    }
    
    
    /**
     Chapter 1
     Dot Product testing.
     Dot procut of two tuples.
     */
    func testDotProdcut() {
        // Given
        let vectorA = newRGVector(x: 1, y: 2, z: 3)
        let vectorB = newRGVector(x: 2, y: 3, z: 4)
        
        let result: Float = 20.0
        
        // Then
        let dotP = dot(vectorA, vectorB)
        print(dotP)
        XCTAssertEqual(dotP.isEqualTo(result), true)
    }
    
    
    /**
     Chapter 1
     Cross Product testing.
     */
    func testCrossProduct() {
        // Given
        let (xA, yA, zA) = (1.0, 2.0, 3.0) as (Float, Float, Float)
        let (xB, yB, zB) = (2.0, 3.0, 4.0) as (Float, Float, Float)
        
        // When
        let vA = newRGVector(x: xA, y: yA, z: zA)
        let vB = newRGVector(x: xB, y: yB, z: zB)
        
        // Then
        let crossAB = cross(lT: vA, rT: vB)
        let crossBA = cross(lT: vB, rT: vA)
        
        XCTAssertEqual(equalTuples(crossAB, newRGVector(x: -1, y: 2, z: -1)), true)
        XCTAssertEqual(equalTuples(crossBA, newRGVector(x:  1, y:-2, z:  1)), true)
    }
    
    
    /*****************************
     * CHAPTER 2
     *****************************/
    
    
    /**
     Chapter 2.
     Test creation of color tuple.
     */
    func testRGColorCreation() {
        // Given
        let r: Float = -0.5
        let g: Float =  0.4
        let b: Float =  1.7
        
        // When
        let c = newRGColor(red: r, green: g, blue: b)
        
        // Then
        XCTAssertEqual(c.red.isEqual(to:  -0.5), true)
        XCTAssertEqual(c.green.isEqual(to: 0.4), true)
        XCTAssertEqual(c.blue.isEqual(to:  1.7), true)
    }
    
    
    /**
     Chapter 2.
     Adding is same as for RGPoints and RGVectors.
     */
    func testRGColorAdding() {
        // Given
        let c1 = newRGColor(red: 0.9, green: 0.6, blue: 0.75)
        let c2 = newRGColor(red: 0.7, green: 0.1, blue: 0.25)
        
        // When
        let res = (c1 + c2)
        let test = newRGColor(red: 1.6, green: 0.7, blue: 1.0)

        // Then
        XCTAssertEqual(equalColors(res, test), true)
    }
    
    
    /**
     Chapter 2.
     Subtracting is same as for RGPoints and RGVectors.
     */
    func testRGColorSubtracting() {
        // Given
        let c1 = newRGColor(red: 0.9, green: 0.6, blue: 0.75)
        let c2 = newRGColor(red: 0.7, green: 0.1, blue: 0.25)
        
        // When
        let res = (c1 - c2)
        let test = newRGColor(red: 0.2, green: 0.5, blue: 0.5)
        
        // Then
        XCTAssertEqual(equalColors(res, test), true)
    }
    
    
    /**
     Chapter 2.
     Multiplying RGColor by scalar.
     */
    func testRGColorMultiplyByScalar() {
        // Given
        let c = newRGColor(red: 0.2, green: 0.3, blue: 0.4)
        
        // When
        // Then
        XCTAssertEqual(equalColors(c * 2, newRGColor(red: 0.4, green: 0.6, blue: 0.8)), true)
    }
    
    
    /**
     Chapter 2.
     Multiplying RGColor by another RGColor.
     The Hadamard product (or Shur product)
     */
    func testRGColorMultiplyByRGColor() {
        // Given
        let c1 = newRGColor(red: 1, green: 0.2, blue: 0.4)
        let c2 = newRGColor(red: 0.9, green: 1, blue: 0.1)
        
        // When
        // Then
        let c3 = c1 * c2
        
        XCTAssertEqual(equalColors(c3, newRGColor(red: 0.9, green: 0.2, blue: 0.04)), true)
    }
    
    
    /**
     Chapter 2.
     Writing to canvas tests.
     */
    func testCreatingCanvas() {
        // Given
        let w = 10
        let h = 20
        let clr = newRGColorBlack()
        
        // When
        let c = newRGCanvas(width: w, height: h)
        
        // Then
        
        XCTAssertEqual(c.size.width, 10)
        XCTAssertEqual(c.size.height, 20)
        
        XCTAssertNil(c.findPositionOfUnequal(clr))
    }
    
    
    /**
     Chapter 2.
     Testing writing pixels to a canvas.
     */
    func testWritePixelsToACanvas() {
        // Given
        var c = newRGCanvas(width: 10, height: 20)
        let red = newRGColor(red: 1, green: 0, blue: 0)
        
        // When
        _ = writePixelToRGCanvas(&c, position: RGPosition(x: 2, y: 3), color: red)
        let pixel = c.pixelAt(RGPosition(x: 2, y: 3))
        
        // Then
        print(pixel)
        XCTAssertEqual(equalColors(pixel, red), true)
    }
    
    
    /**
     Chapter 2.
     Writing canvas to PPM image file test.
     */
    func testCanvasPPMFileHeader() {
        // Given
        let c = newRGCanvas(width: 5, height: 3)
        
        // When
        let ppmFile = c.canvasHeaderForASCIIFormat()
        
        let header =
"""
P3
5 3
255
"""
        print(header)
        print(ppmFile)
        // Then
        XCTAssertEqual(ppmFile == header , true)
    }
    
    /**
     Chapter 2.
     Tests the writing of three pixel on canvas and saving the canvas on a file.
     */
    func testPPMPixelData() {
        // Given
        var c = newRGCanvas(width: 5, height: 3)
        let clr1 = newRGColor(red: 1.5, green: 0, blue: 0)
        let clr2 = newRGColor(red: 0, green: 0.5, blue: 0)
        let clr3 = newRGColor(red: -0.5, green: 0, blue: 1)
        
        // When
        _ = c.writePixelAt(RGPosition(x: 0, y: 0), color: clr1)
        _ = c.writePixelAt(RGPosition(x: 2, y: 1), color: clr2)
        _ = c.writePixelAt(RGPosition(x: 4, y: 2), color: clr3)
        
        let ppmFile = canvasToPPMString(c)
        
        // Because the books example file string is not in the same form as ours since
        // we have already implemented the next demand, the 70 char limit we write the file down and
        // check it visually
    
        /**
         Comment next sectio if you don't want to save the file on destop.
         */
        let home = URL(fileURLWithPath: NSHomeDirectory())
        let url = home.appendingPathComponent("Desktop/TRTC_image1.ppm")
        print(url)
        do {
            try ppmFile.write(to: url, atomically: false, encoding: .utf8)
        } catch {
            print(error)
        }
    }
    
    
    func testLongLineSplitting() {
        // Given
        var c = newRGCanvas(width: 10, height: 2)
        let clr = newRGColor(red: 1, green: 0.8, blue: 0.6)
        
        c.setNewBaseColor(clr)
        
        // When
        let ppmFile = canvasToPPMString(c)
        
        /**
         Comment next sectio out and it will write the file to your desktop.
         */
        let home = URL(fileURLWithPath: NSHomeDirectory())
        let url = home.appendingPathComponent("Desktop/TRTC_image2.ppm")
        print(url)
        do {
            try ppmFile.write(to: url, atomically: false, encoding: .utf8)
        } catch {
            print(error)
        }
    }
    
    
    func testLastChar() {
        // Given
        let c = newRGCanvas(width: 5, height: 3)
        
        // When
        let ppmFile = canvasToPPMString(c)
        
        // Then
        XCTAssertEqual(ppmFile.last?.isNewline, true)
    }
    
    
    // This is example function not implemented yet.
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
