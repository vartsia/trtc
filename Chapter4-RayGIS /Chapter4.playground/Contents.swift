//
//  MIT License
//  Copyright (c) 2019 Juhani Nikumaa
//  vartsia.net
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
import Cocoa
import RGCore
import simd


 /**
 The Ray Tracing Challenge
 Chapter 4 - Putting it together challend at the end of the chapter.
 Here we create clock face using point and transfomation matrices.
 */


// Create the canvas for the clock.
var c = newRGCanvas(width: 400, height: 400)


// I choose to use green as the color for the hours in clock face.
var clr = newRGColor(red: 0, green: 1.0, blue: 0)


// The center of the canvas.
let center = newRGPoint(x: 200, y: 200, z: 0)


// Twelve hours so we need a loop to create twelve points.
for i in 0..<12 {
    // Point to start with.
    var p = newRGPoint(x: 0, y: 1, z: 0)

    // This moves the point to center.
    let T = newRGTranslationMatrix(x: center.x, y: center.y, z: center.z)
    
    // Scales the clock.
    let S = newRGScalingMatrix(float3(x: 100, y: 100, z: 0))
    
    // Size of the rotation per hour.
    let hourStep = Float.pi / Float(6)
    
    // This rotates the hours around the clock face.
    let R = newRGRotationZ(Float(i) * hourStep )
    
    // Calculates the position of the current hour on clock face.
    p = T * S * R * p
    
    // Gets the 2D position for canvas.
    let pos = p.getRGPosition()
    let pixelPosition = RGPosition(pos.x, pos.y)
    
    // Print the values so its easier to see what actually happens under cover.
    print(pixelPosition.description)
    
    // When we got the positon we write it into canvas.
    c.writePixelAt(pixelPosition, color: clr)
}


// Generates the ppm ASCII file string.
let ppmFile = canvasToPPMString(c)

// Write the clock face into the canvas.
let home = URL(fileURLWithPath: NSHomeDirectory())
let url = home.appendingPathComponent("Desktop/TRTC_Chapter4_Clock.ppm")
print(url)
do {
    try ppmFile.write(to: url, atomically: false, encoding: .utf8)
} catch {
    print(error)
}
