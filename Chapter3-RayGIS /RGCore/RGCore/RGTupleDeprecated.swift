//
//  Tuple.swift
//  RayGIS
//
//  Created by Juhani Nikumaa on 28/01/2019.
//
//  MIT License
//  Copyright (c) 2019 Juhani Nikumaa
//  vartsia.net
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

//  Based on the book "The Ray Tracer Challenge" written by Jamis Buck

import Foundation


/**
 RGTuple is the core class to hold points and vectors used in 3D space.
 */
public struct RGTupleDeprecated: CustomStringConvertible  {
    public var description: String {
        return "x \(x) y \(y) z \(z) w \(w)"
    }
    
    internal var isColor: Bool
    internal var x: Float = 0.0
    internal var y: Float = 0.0
    internal var z: Float = 0.0
    internal var w: Float = 0.0
    internal var red: Float {
        get {
            return self.x
        }
    }
    internal var green: Float {
        get {
            return self.y
        }
    }
    internal var blue: Float {
        get {
            return self.z
        }
    }
    
    
    init(x: Float, y: Float, z: Float, w: Float, isColor: Bool = false) {
        self.x = x
        self.y = y
        self.z = z
        self.w = w
        self.isColor = isColor
    }
    
    
    /**
     isPoint and isVector where previously global functions.
     For convinience functions are added inside the struct also.

     Test if RGTuple is point.
     - Returns: **true** if the w component of RGTuple is 1.0
     */
    public func isPoint() -> Bool {
        return !isColor && self.w.isEqual(to: 1.0)
    }
    
    
    /**
     Test if RGTuple is vector.
     - Returns: **true** if the w component of RGTuple is 0.0
     */
    public func isVector() -> Bool {
        return !isColor && self.w.isEqual(to: 0.0)
    }
    
    
    /**
     Compares this tuple to tuple that is given as argument.
     Every matching component inside tupels are compared against each other.
     Internaly the Float isEqual method is used.
     - Returns: Bool, **true** if equal and **false** otherwise.
     */
    public func isEqualToStrict(_ tuple: RGTupleDeprecated) -> Bool {
        if self.x.isEqual(to: tuple.x) &&
            self.y.isEqual(to: tuple.y) &&
            self.z.isEqual(to: tuple.z) &&
            self.w.isEqual(to: tuple.w) {
            return true
            }
        else {
            return false
            }
    }
    
    
    /**
     Another method to test equality.
     This uses the custom Float extensioin method isEqualTo to
     compare the indivudual components.
     You can give the desired epsilon when needed.
     - Returns: Bool, **true** if equal based on the **epsilon** and **false** otherwise.
     */
    public func isEqualTo(_ tuple: RGTupleDeprecated, epsilon e: Float = 0.000001) -> Bool {
        if  self.x.isEqualTo(tuple.x, eps: e) &&
            self.y.isEqualTo(tuple.y, eps: e) &&
            self.z.isEqualTo(tuple.z, eps: e) &&
            self.w.isEqualTo(tuple.w, eps: e) {
            return true
        }
        else {
            return false
        }
    }
    
    
    /**
     - Returns: Void. Affect the object it self.
     Negates all the components of the tuple.
     */
    public mutating func negate() {
        self.x.negate()
        self.y.negate()
        self.z.negate()
        self.w.negate()
    }
    
    
    /**
     - Returns: The magnitude of tuple aka lenght.
     */
    public func magnitude() -> Float {
        let sum = powf(self.x, 2) + powf(self.y, 2) + powf(self.z, 2) + powf(self.w, 2)
        return sqrtf(sum)
    }
    
    
    /**
     - Returns: Normalized version of the tuple.
     */
    public func normalize() -> RGTupleDeprecated {
        let m = self.magnitude()
        return RGTupleDeprecated(x: self.x / m , y: self.y / m, z: self.z / m, w: self.w / m)
    }
}


/**
 FUNCTIONS
 */

/**
 - Returns: A new tuple if adding is succesfull and the left operand unchanged if adding makes no sense.
 */
public func +(lTuple: RGTupleDeprecated, rTuple: RGTupleDeprecated) -> RGTupleDeprecated {
    if (isVector(lTuple) && isPoint(rTuple) ) ||
        (isPoint(lTuple) && isVector(rTuple)) ||
        (isVector(lTuple) && isVector(rTuple)) ||
        (lTuple.isColor && rTuple.isColor) {
        if (lTuple.isColor && rTuple.isColor) {
            return RGTupleDeprecated(x: lTuple.x + rTuple.x, y: lTuple.y +  rTuple.y, z: lTuple.z + rTuple.z, w: 1.0)
            }
        else {
            return RGTupleDeprecated(x: lTuple.x + rTuple.x, y: lTuple.y +  rTuple.y, z: lTuple.z + rTuple.z , w: lTuple.w + rTuple.w)
            }
        }
    else {
        return lTuple
        }
}


/**
 - Returns: A new tuple if subtracting is succesfull and the left operand unchanged if subtracting makes no sense.
 */
public func -(lTuple: RGTupleDeprecated, rTuple: RGTupleDeprecated) -> RGTupleDeprecated {
    if (isVector(lTuple) && isVector(rTuple) ) ||
        (isPoint(lTuple) && isPoint(rTuple)) ||
        (isPoint(lTuple) && isVector(rTuple)) {
        return RGTupleDeprecated(x: lTuple.x - rTuple.x, y: lTuple.y -  rTuple.y, z: lTuple.z - rTuple.z , w: lTuple.w - rTuple.w)
        }
    else {
        return lTuple
        }
}


/**
 - Returns: A new tuple where all components are inverted.
 */
public prefix func -(_ t: RGTupleDeprecated) -> RGTupleDeprecated {
    return RGTupleDeprecated(x: -t.x, y: -t.y, z: -t.z, w: -t.w)
}


/**
 - Returns: A new tuple multiplied with scalar.
 */
public func *(_ s: Float, t: RGTupleDeprecated) -> RGTupleDeprecated {
    return RGTupleDeprecated(x: s * t.x, y: s * t.y, z: s * t.z, w: s * t.w)
}


/**
 - Returns: A new tuple divided by scalar.
 */
public func /(_ t: RGTupleDeprecated, s: Float) -> RGTupleDeprecated {
    if !s.isEqual(to: 0.0) {
        return RGTupleDeprecated(x: t.x / s, y: t.y / s, z: t.z / s, w: t.w / s)
        }
    else {
        return t
        }
}


/**
 Calculates the dot product of given vectors.
 Read the Joe asks: sectino in the TRTC to know more about w component.
 - Returns: A scalar value representig the dot product of the two given vectors.
 */
public func dot(_ v1: RGTupleDeprecated, _ v2: RGTupleDeprecated) -> Float {
    return (v1.x * v2.x) + (v1.y * v2.y) + (v1.z * v2.z) + (v1.w * v2.w)
}


/**
 Calculates the cross product of two given vectors.
 - Returns: RGTuple that is vector.
 */
public func cross(_ v1: RGTupleDeprecated, _ v2: RGTupleDeprecated) -> RGTupleDeprecated {
    return newVector(x: (v1.y * v2.z) - (v1.z * v2.y),
                     y: (v1.z * v2.x) - (v1.x * v2.z),
                     z: (v1.x * v2.y) - (v1.y * v2.x)
    )
}


/// *****************
/// FACTORY FUNCTIONS
/// *****************

/**
 Factory function to create a RGTuple of type POINT.
 */
public func newPoint(x: Float, y: Float, z: Float) -> RGTupleDeprecated {
    let point = RGTupleDeprecated(x: x, y: y, z: z, w: 1.0)
    return point
}


/**
 Factory function to create a RGTuple of type VECTOR.
 */
public func newVector(x: Float, y: Float, z: Float) -> RGTupleDeprecated {
    let vector = RGTupleDeprecated(x: x, y: y, z: z, w: 0.0)
    return vector
}


/**
 Factory function to create a RGTuple of type COLOR.
 */
public func newColor(red: Float, green: Float, blue: Float) -> RGTupleDeprecated {
    var color = RGTupleDeprecated(x: red, y: green, z: blue, w: 1.0)
    color.isColor = true
    return color
}


/// **********************************
/// TEST FUNCTIONS FOR TUPLE PROPERTIS
/// **********************************


/**
 Test if RGTuple is point.
 - Parameter tuple: RGTuple item.
 - Returns: **true** if the w component of RGTuple is 1.0
 */
public func isPoint(_ tuple: RGTupleDeprecated) -> Bool {
    return tuple.w.isEqual(to: 1.0)
}


/**
 Test if RGTuple is vector.
 - Parameter tuple: RGTuple item.
 - Returns: **true** if the w component of RGTuple is 0.0
 */
public func isVector(_ tuple: RGTupleDeprecated) -> Bool {
    return tuple.w.isEqual(to: 0.0)
}
