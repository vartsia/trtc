//
// RGMatrixExtensions.swift
// RGCore
//
// Created by Juhani Nikumaa on 07/04/2019
//
// MIT License
// Copyright (c) 2019 Juhani Nikumaa
// vartsia.net
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.


import Foundation
import simd


public extension RGMatrix3x3 {
    func isEqualTo(_ m: RGMatrix3x3) -> Bool {
        if self.columns.0.isEqualTo(m.columns.0) && self.columns.1.isEqualTo(m.columns.1) && self.columns.2.isEqualTo(m.columns.2) {
            return true
            }
        else {
            return false
            }
    }
    
    
    func submatrix(c columnToDelete: RGPosition3, r rowToDelete: RGPosition3) -> RGMatrix2x2 {
        switch columnToDelete {
        case .First:
            return RGMatrix2x2(self.columns.1.removeRow(rowToDelete), self.columns.2.removeRow(rowToDelete))
        case .Second:
            return RGMatrix2x2(self.columns.0.removeRow(rowToDelete), self.columns.2.removeRow(rowToDelete))
        case .Third:
            return RGMatrix2x2(self.columns.0.removeRow(rowToDelete), self.columns.1.removeRow(rowToDelete))
        }
    }
    
    
    func minor(c columnToDelete: RGPosition3, r rowToDelete: RGPosition3) -> Double {
        return self.submatrix(c: columnToDelete, r: rowToDelete).determinant
    }
    
    
    func cofactor(c columnToDelete: RGPosition3, r rowToDelete: RGPosition3) -> Double {
        let minor = self.minor(c: columnToDelete, r: rowToDelete)
        if (columnToDelete.rawValue + rowToDelete.rawValue) % 2 == 0 {
            return minor
            }
        else {
            return -minor
            }
    }
}


public extension RGMatrix4x4 {
    func isEqualTo(_ m: RGMatrix4x4) -> Bool {
        if self.columns.0.isEqualTo(m.columns.0) && self.columns.1.isEqualTo(m.columns.1) && self.columns.2.isEqualTo(m.columns.2) && self.columns.3.isEqualTo(m.columns.3) {
            return true
        }
        else {
            return false
        }
    }
    
    
    /**
     Method to create idnetity matrix where all values in diagonal are 1.
     | 1 | 0 | 0 | 0
     | 0 | 1 | 0 | 0
     | 0 | 0 | 1 | 0
     | 0 | 0 | 0 | 1
     */
    static func identity() -> RGMatrix4x4 {
        return RGMatrix4x4.init(diagonal: RGTuple(repeating: 1))
    }
    
    
    func submatrix(c columnToDelete: RGPosition4, r rowToDelete: RGPosition4) -> RGMatrix3x3 {
        switch columnToDelete {
        case .First:
            return RGMatrix3x3(self.columns.1.removeRow(rowToDelete), self.columns.2.removeRow(rowToDelete), self.columns.3.removeRow(rowToDelete))
        case .Second:
            return RGMatrix3x3(self.columns.0.removeRow(rowToDelete), self.columns.2.removeRow(rowToDelete), self.columns.3.removeRow(rowToDelete))
        case .Third:
            return RGMatrix3x3(self.columns.0.removeRow(rowToDelete), self.columns.1.removeRow(rowToDelete), self.columns.3.removeRow(rowToDelete))
        case .Fourth:
            return RGMatrix3x3(self.columns.0.removeRow(rowToDelete), self.columns.1.removeRow(rowToDelete), self.columns.2.removeRow(rowToDelete))
        }
    }
    
    
    func minor(c columnToDelete: RGPosition4, r rowToDelete: RGPosition4) -> Double {
        return self.submatrix(c: columnToDelete, r: rowToDelete).determinant
    }
    
    
    func cofactor(c columnToDelete: RGPosition4, r rowToDelete: RGPosition4) -> Double {
        let minor = self.minor(c: columnToDelete, r: rowToDelete)
        if (columnToDelete.rawValue + rowToDelete.rawValue) % 2 == 0 {
            return minor
        }
        else {
            return -minor
        }
    }
}
