//
// RGRay.swift
// RGCore
//
// Created by Juhani Nikumaa on 07/05/2019
//
// MIT License
// Copyright (c) 2019 Juhani Nikumaa
// vartsia.net
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.


import Foundation
import simd


/**
 RGRay struct.
 Origin is the rays starting point and
 direction is where the ray is pointing to.
 */
public struct RGRay {
    public var origin: RGTuple
    public var direction: RGTuple
}


/**
 This is for convinience to creare a ray.
 */
public func newRGRay(origin: RGTuple, direction: RGTuple) -> RGRay {
    return RGRay(origin: origin, direction: direction)
}


/**
 This is for convinience to creare a ray.
 */
public func newRGPosition(ray: RGRay, t: Double) -> RGTuple {
    return ray.origin + (ray.direction * t)
}

/**
 - Parameter r: RGRay to be transfomed.
 - Parameter matrix: The 4x4 transformation matrix.
 - Returns: New transformed RGRay.
 */
public func transformRGRay(_ r: RGRay, matrix m: RGMatrix4x4) -> RGRay {
    // Here we force the origin to be a point.
    var o = m * r.origin
    o.w = 1.0
    
    // Here we force the direction to be a vector.
    var d = m * r.direction
    d.w = 0.0
    
    return newRGRay(origin: o, direction: d)
}
