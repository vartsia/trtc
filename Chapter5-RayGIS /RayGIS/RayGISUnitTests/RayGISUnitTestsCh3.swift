//
// RayGISUnitTestsCh3.swift
// RayGISUnitTests
//
// Created by Juhani Nikumaa on 02/04/2019
//
// MIT License
// Copyright (c) 2019 Juhani Nikumaa
// vartsia.net
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.


import XCTest
import AppKit
import simd
@testable import RGCore


class RayGISUnitTestsCh3: XCTestCase {

    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    
    /**
     Chapter 3.
     1st test case.
     Create 4x4 matrix.
     */
    func testRGMatrix4x4Creation() {
        // Given
        // This is the matrix we are trying to create and test that the creation sucseeds.
        // |  1   |  2   |  3   |  4   |
        // |  5.5 |  6.5 |  7.5 |  8.5 |
        // |  9   | 10   | 11   | 12   |
        // | 13.5 | 14.5 | 15.5 | 16.5 |
        
        // When
        // Since our matrix is column based we create 4 RGTuple variables using values from the columns above.
        let c1 = RGTuple(x: 1, y: 5.5, z: 9, w: 13.5)
        let c2 = RGTuple(x: 2, y: 6.5, z: 10, w: 14.5)
        let c3 = RGTuple(x: 3, y: 7.5, z: 11, w: 15.5)
        let c4 = RGTuple(x: 4, y: 8.5, z: 12, w: 16.5)
        let m4x4 = RGMatrix4x4(c1, c2, c3, c4)
        
        // Then
        XCTAssertEqual(m4x4.columns.0.x == 1, true)
        XCTAssertEqual(m4x4.columns.3.x == 4, true)
        XCTAssertEqual(m4x4.columns.0.y == 5.5, true)
        XCTAssertEqual(m4x4.columns.2.y == 7.5, true)
        XCTAssertEqual(m4x4.columns.2.z == 11, true)
        XCTAssertEqual(m4x4.columns.0.w == 13.5, true)
        
    }
    
    
    /**
     Chapter 3.
     2nd test case.
     2x2 matrix.
     */
    func testRGMatrix2x2Creation() {
        // Given
        // This is the matrix we are trying to create and test that the creation sucseeds.
        // |  -3  |   5  |
        // |   1  |  -2  |

        // When
        // Since our matrix is column based we create 4 RGTuple variables using values from the columns above.
        let m2x2 = RGMatrix2x2(simd_double2(x: -3, y: 1), simd_double2(x: 5, y: -2))

        
        // Then
        XCTAssertEqual(m2x2.columns.0.x == -3, true)
        XCTAssertEqual(m2x2.columns.0.y ==  1, true)
        XCTAssertEqual(m2x2.columns.1.x ==  5, true)
        XCTAssertEqual(m2x2.columns.1.y == -2, true)
    }
    
    
    /**
     Chapter 3.
     3rd test case.
     3x3 matrix.
     */
    func testRGMatrix3x3Creation() {
        // Given
        // This is the matrix we are trying to create and test that the creation sucseeds.
        // | -3 |  5 |  0 |
        // |  1 | -2 | -7 |
        // |  0 |  1 |  1 |
        
        // When
        // Since our matrix is column based we create 4 RGTuple variables using values from the columns above.
        let m3x3 = RGMatrix3x3(simd_double3(x: -3, y: 1, z: 0), simd_double3(x: 5, y: -2, z: 1), simd_double3(x: 0, y: -7, z: 1))
        
        
        // Then
        XCTAssertEqual(m3x3.columns.0.x == -3, true)
        XCTAssertEqual(m3x3.columns.1.y == -2, true)
        XCTAssertEqual(m3x3.columns.2.z ==  1, true)
        
    }
    
    
    /**
     Chapter 3.
     4th test case.
     Compare matrixes.
     */
    func testRGMatrixComparison() {
        // Given matrix A
        // | 1 | 2 | 3 | 4 |
        // | 5 | 6 | 7 | 8 |
        // | 9 | 8 | 7 | 6 |
        // | 5 | 4 | 3 | 2 |
        
        // And​ the following matrix B:
        // | 2 | 3 | 4 | 5 |
        // | 6 | 7 | 8 | 9 |
        // | 8 | 7 | 6 | 5 |
        // | 4 | 3 | 2 | 1 |
        
        // When
        let mA = RGMatrix4x4(simd_double4(x: 1, y: 5, z: 9, w: 5), simd_double4(x: 2, y: 6, z: 8, w: 4), simd_double4(x: 3, y: 7, z: 7, w: 3), simd_double4(x: 4, y: 8, z: 6, w: 2))
        let mB = RGMatrix4x4(simd_double4(x: 2, y: 6, z: 8, w: 4), simd_double4(x: 3, y: 7, z: 7, w: 3), simd_double4(x: 4, y: 8, z: 6, w: 2), simd_double4(x: 5, y: 9, z: 5, w: 1))
        
        // Then
        XCTAssertEqual(simd_equal(mA, mA), true)
        XCTAssertEqual(simd_equal(mA, mB), false)
    }
    
    
    /**
     Chapter 3.
     5th test case.
     Multiplying the 4x4 matrix with 4x4 matrix.
     */
    func testRGMatrix4x4ByMatrixMultiplicaiton() {
        /*
        // Given the following matrix m1:
        // | 1 | 2 | 3 | 4 |
        // | 5 | 6 | 7 | 8 |
        // | 9 | 8 | 7 | 6 |
        // | 5 | 4 | 3 | 2 |
        ​// And​ the following matrix m2:
        ​// | -2 | 1 | 2 |  3 |
        ​// |  3 | 2 | 1 | -1 |
        ​// |  4 | 3 | 6 |  5 |
        ​// |  1 | 2 | 7 |  8 |
        ​// Then​ m1 * m2 is the following 4x4 matrix:
        ​// | 20|  22 |  50 |  48 |
        ​// | 44|  54 | 114 | 108 |
        ​// | 40|  58 | 110 | 102 |
        ​// | 16|  26 |  46 |  42 |
         */
        
        let m1 = RGMatrix4x4(RGTuple(x: 1, y: 5, z: 9, w: 5),
                           RGTuple(x: 2, y: 6, z: 8, w: 4),
                           RGTuple(x: 3, y: 7, z: 7, w: 3),
                           RGTuple(x: 4, y: 8, z: 6, w: 2)
        )
        let m2 = RGMatrix4x4(RGTuple(x: -2, y: 3, z: 4, w: 1),
                           RGTuple(x: 1, y: 2, z: 3, w: 2),
                           RGTuple(x: 2, y: 1, z: 6, w: 7),
                           RGTuple(x: 3, y: -1, z: 5, w: 8)
        )
        let test = RGMatrix4x4(RGTuple(x: 20, y: 44, z: 40, w: 16),
                             RGTuple(x: 22, y: 54, z: 58, w: 26),
                             RGTuple(x: 50, y: 114, z: 110, w: 46),
                             RGTuple(x: 48, y: 108, z: 102, w: 42)
        )

        let m1x2 = m1 * m2
        XCTAssertEqual(simd_equal(m1x2, test), true)
        print(m1x2)
    }
    
    
    /**
     Chapter 3.
     6th test case.
     Multiply the 4x4 matrix by tuple.
     */
    func testRGMatrix4x4ByTupleMultiplicaiton() {
        // Given
        // | 1 | 2 | 3 | 4 |
        // | 2 | 4 | 4 | 2 |
        // | 8 | 6 | 4 | 1 |
        // | 0 | 0 | 0 | 1 |
        
        // When
        
        let M = RGMatrix4x4(
            RGTuple(x: 1, y: 2, z: 8, w: 0),
            RGTuple(x: 2, y: 4, z: 6, w: 0),
            RGTuple(x: 3, y: 4, z: 4, w: 0),
            RGTuple(x: 4, y: 2, z: 1, w: 1)
        )
        let t = RGTuple(x: 1, y: 2, z: 3, w: 1)
        
        // Then
        XCTAssertEqual(simd_equal((M * t), RGTuple(x: 18, y: 24, z: 33, w: 1)), true)
        
    }
    
    
    /**
     Chapter 3.
     7th test case.
     Multiplying with identity matrix.
     */
    func testIdentityMatrix() {
        // Given
        // | 0 | 1 |  2 |  4 |
        // | 1 | 2 |  4 |  8 |
        // | 2 | 4 |  8 | 16 |
        // | 4 | 8 | 16 | 32 |
        
        // When
        // Tuples could be created first as variables and then use those in the RGMAtrix4x4 init as parameters.
        let c1 = RGTuple(x: 0, y: 1, z: 2, w: 4)
        let c2 = RGTuple(x: 1, y: 2, z: 4, w: 8)
        let c3 = RGTuple(x: 2, y: 4, z: 8, w: 16)
        let c4 = RGTuple(x: 4, y: 8, z: 16, w: 32)
        
        let M = RGMatrix4x4(columns: (c1, c2, c3, c4))
        let iM = RGMatrix4x4.init(diagonal: RGTuple(repeating: 1))
        
        // Then
        let rM = iM * M
        
        XCTAssertEqual(simd_equal(rM, M), true)
    }
    
    
    /**
     Chapter 3.
     8th test case.
     Transposing matrix.
     */
    func testMatrixTransposing() {
        // Given
        // | 0 | 9 | 3 | 0 |
        // | 9 | 8 | 0 | 8 |
        // | 1 | 8 | 5 | 3 |
        // | 0 | 0 | 5 | 8 |
        // Here the tupels are created and placed directly in the RGMatrix4x4 init call.
        let M = RGMatrix4x4(
            RGTuple(x: 0, y: 9, z: 1, w: 0),
            RGTuple(x: 9, y: 8, z: 8, w: 0),
            RGTuple(x: 3, y: 0, z: 5, w: 5),
            RGTuple(x: 0, y: 8, z: 3, w: 8)
        )
        
        // When
        // | 0 | 9 | 1 | 0 |
        // | 9 | 8 | 8 | 0 |
        // | 3 | 0 | 5 | 5 |
        // | 0 | 8 | 3 | 8 |
        let tM = RGMatrix4x4(
            RGTuple(x: 0, y: 9, z: 3, w: 0),
            RGTuple(x: 9, y: 8, z: 0, w: 8),
            RGTuple(x: 1, y: 8, z: 5, w: 3),
            RGTuple(x: 0, y: 0, z: 5, w: 8)
        )
        
        // Then
        XCTAssertEqual(simd_equal(tM, simd_transpose(M)), true)
    }
    
    
    /**
     Chapter 3.
     9th test case.
     Identity matrix transposing. Returns it self allways.
     */
    func testIndentityMatrixTransposing() {
        // Given
        let M = RGMatrix4x4.identity()
        
        // Then
        XCTAssertEqual(simd_equal(M.transpose, M), true)
        
    }

    // *******************
    // Inverting matrices.
    // *******************
    
    /**
     Chapter 3.
     10th test case.
     Determinant of 2x2 matrix.
     */
    func testDeterminantMatrices() {
        // Given
        let m = RGMatrix2x2(double2(x: 1, y: -3), double2(x: 5, y: 2))
        
        // Then
        XCTAssertEqual(m.determinant == 17, true)
    }
    
    /**
     Chapter 3.
     11th test case.
     Submatrices from 3x3 to 2x2 matrix.
     */
    func testSubmatrices3x3to2x2() {
        // When
        // |  1 | 5 |  0 |
        // | -3 | 2 |  7 |
        // |  0 | 6 | -3 |
        let M = RGMatrix3x3(
            double3(x: 1, y: -3, z: 0),
            double3(x: 5, y: 2, z: 6),
            double3(x: 0, y: 7, z: -3)
        )
        
        let sub = RGMatrix2x2(double2(x: -3, y: 0), double2(x: 2, y: 6))
        
        // Then
        let subM = M.submatrix(c: .Third, r: .First)
        // | -3 | 2 |
        // |  0 | 6 |
        XCTAssertEqual(simd_equal(sub, subM) , true)
    }
    
    /**
     Chapter 3.
     12th test case.
     Submatrices from 4x4 to 3x3 matrix.
     */
    func testSubmatrices4x4to3xs3() {
		// Given
        // Matrix M
        // | -6 |  1 |  1 |  6 |
		// | -8 |  5 |  8 |  6 |
        // | -1 |  0 |  8 |  2 |
		// | -7 |  1 | -1 |  1 |
		
        // Submatrix of M
        // | -6 |  1 | 6 |
        // | -8 |  8 | 6 |
        // | -7 | -1 | 1 |
        
		// When
		let M = RGMatrix4x4(rows: [RGTuple(x: -6, y: 1, z: 1, w: 6),
                                   RGTuple(x: -8, y: 5, z: 8, w: 6),
                                   RGTuple(x: -1, y: 0, z: 8, w: 2),
                                   RGTuple(x: -7, y: 1, z: -1, w: 1)]
        )
        
        let sub = RGMatrix3x3(double3(x: -6, y: -8, z: -7), double3(x: 1, y: 8, z: -1), double3(x: 6, y: 6, z: 1))
        let subM = M.submatrix(c: .Second, r: .Third)
        
        // Then
        XCTAssertEqual(simd_equal(sub, subM), true)
    }
    
    
    /**
     Chapter 3.
     13th test case.
     Matrix minor.
     */
    func testMatrixMinor() {
        // Given
        // |  3 |  5 |  0 |
        // |  2 | -1 | -7 |
        // |  6 | -1 |  5 |
        
        // When
        let M = RGMatrix3x3(double3(x: 3, y: 2, z: 6), double3(x: 5, y: -1, z: -1), double3(x: 0, y: -7, z: 5))
        let subM = M.submatrix(c: .First, r: .Second)
        
        // Then
        XCTAssertEqual(subM.determinant == 25, true)
        XCTAssertEqual(M.minor(c: .First, r: .Second) == 25, true)
    }
    
    
    /**
     Chapter 3.
     14th test case.
     Matrix cofactor.
     */
    func testMatrixCofactor() {
        // Given
        // |  3 |  5 |  0 |
        // |  2 | -1 | -7 |
        // |  6 | -1 |  5 |
        
        // When
        let M = RGMatrix3x3(double3(x: 3, y: 2, z: 6), double3(x: 5, y: -1, z: -1), double3(x: 0, y: -7, z: 5))
        
        // Then
        XCTAssertEqual(M.minor(c: .First, r: .First) == -12, true)
        XCTAssertEqual(M.cofactor(c: .First, r: .First) == -12, true)
        XCTAssertEqual(M.minor(c: .First, r: .Second) == 25, true)
        XCTAssertEqual(M.cofactor(c: .First, r: .Second) == -25, true)
    }
    

    /**
     Chapter 3.
     15th test case.
     Determinatn of 3x3 and 4x4 matrices.
     Use simd build-in determinant method.
     */
    func testMatrix3x3and4x4Determinant() {
        // Given 3x3 Matrix
        // |  1 |  2 |  6 |
        // | -5 |  8 | -4 |
        // |  2 |  6 |  4 |
        
        // When
        let Ma = RGMatrix3x3(double3(x: 1, y: -5, z: 2), double3(x: 2, y: 8, z: 6), double3(x: 6, y: -4, z: 4))
        
        // Then
        XCTAssertEqual(Ma.cofactor(c: .First, r: .First) == 56, true)
        XCTAssertEqual(Ma.cofactor(c: .Second, r: .First) == 12, true)
        XCTAssertEqual(Ma.cofactor(c: .Third, r: .First) == -46, true)
        XCTAssertEqual(Ma.determinant == -196, true)
        
        // Given 4x4 Matrix
        // | -2 | -8 |  3 |  5 |
        // | -3 |  1 |  7 |  3 |
        // |  1 |  2 | -9 |  6 |
        // | -6 |  7 |  7 | -9 |
        
        // When
        let Mb = RGMatrix4x4(RGTuple(x: -2, y: -3, z: 1, w: -6),
                             RGTuple(x: -8, y: 1, z: 2, w: 7),
                             RGTuple(x: 3, y: 7, z: -9, w: 7),
                             RGTuple(x: 5, y: 3, z: 6, w: -9)
        )
        
        // Then
        XCTAssertEqual(Mb.cofactor(c: .First, r: .First) == 690, true)
        XCTAssertEqual(Mb.cofactor(c: .Second, r: .First) == 447, true)
        XCTAssertEqual(Mb.cofactor(c: .Third, r: .First) == 210, true)
        XCTAssertEqual(Mb.cofactor(c: .Fourth, r: .First) == 51, true)
        XCTAssertEqual(Mb.determinant == -4071, true)
    }
    
    
    /**
     Chapter 3.
     16th test case.
     Test if matrix is invertible.
     */
    func testInvertibility() {
        // Given
        // |  6 |  4 |  4 |  4 |
        // |  5 |  5 |  7 |  6 |
        // |  4 | -9 |  3 | -7 |
        // |  9 |  1 |  7 | -6 |
        
        // When
        let invM = RGMatrix4x4(RGTuple(x: 6, y: 5, z: 4, w: 9),
                               RGTuple(x: 4, y: 5, z: -9, w: 1),
                               RGTuple(x: 4, y: 7, z: 3, w: 7),
                               RGTuple(x: 4, y: 6, z: -7, w: -6)
        )
        
        // Then
        XCTAssertEqual(invM.determinant == -2120, true)
        
        // Given
        // | -4 |  2 | -2 | -3 |
        // |  9 |  6 |  2 |  6 |
        // |  0 | -5 |  1 | -5 |
        // |  0 |  0 |  0 |  0 |
        
        // When
        let nInvM = RGMatrix4x4(RGTuple(x: -4, y: 9, z: 0, w: 0),
                                RGTuple(x: 2, y: 6, z: -5, w: 0),
                                RGTuple(x: -2, y: 2, z: 1, w: 0),
                                RGTuple(x: -3, y: 6, z: -5, w: 0)
        )
        
        // Then
        XCTAssertEqual(nInvM.determinant == 0, true)
    }
    
    
    /**
     Chapter 3.
     16th test case.
     Test if matrix is invertible.
     */
    func testInverseMatrix() {
        // Given
        // | -5 |  2 |  6 | -8 |
        // |  1 | -5 |  1 |  8 |
        // |  7 |  7 | -6 | -7 |
        // |  1 | -3 |  7 |  4 |
        
        // |  0.21805 |  0.45113 |  0.24060 | -0.04511 |
        // | -0.80827 | -1.45677 | -0.44361 |  0.52068 |
        // | -0.07895 | -0.22368 | -0.05263 |  0.19737 |
        // | -0.52256 | -0.81391 | -0.30075 |  0.30639 |
        
        // When
        let M = RGMatrix4x4(RGTuple(x: -5, y:  1,  z:  7, w:  1),
                            RGTuple(x:  2, y: -5,  z:  7, w: -3),
                            RGTuple(x:  6, y:  1,  z: -6, w:  7),
                            RGTuple(x: -8, y:  8,  z: -7, w:  4)
        )
        
        let inv = RGMatrix4x4(RGTuple(x: 0.21805, y: -0.80827, z: -0.07895, w: -0.52256),
                              RGTuple(x: 0.45113, y: -1.45677, z: -0.22368, w: -0.81391),
                              RGTuple(x: 0.24060, y: -0.44361, z: -0.05263, w: -0.30075),
                              RGTuple(x: -0.04511, y: 0.52068, z: 0.19737, w: 0.30639)
        )
        
        print(M.inverse)
        print(inv)
        // Then
        XCTAssertEqual(M.inverse.isEqualTo(inv), true)
        
    }
    
    
    /**
     Chapter 3.
     17th test case.
     Test original iverse case A * B = C so C * ivnerse(B) = A
     */
    func testInverseMultiplicationByProduct() {
        /*
        // Given the 4x4 matric A:
        // |  3 | -9 |  7 |  3 |
        // |  3 | -8 |  2 | -9 |
        // | -4 |  4 |  4 |  1 |
        // | -6 |  5 | -1 |  1 |
        // ​​And​ the following 4x4 matrix B:
        ​// |  8 |  2 |  2 |  2 |
        ​// |  3 | -1 |  7 |  0 |
        ​// |  7 |  0 |  5 |  4 |
        ​// |  6 | -2 |  0 |  5 |
        */
        let A = RGMatrix4x4(RGTuple(x: 3, y: 3, z: -4, w: -6),
                            RGTuple(x: -9, y: -8, z: 4, w: 5),
                            RGTuple(x: 7, y: 2, z: 4, w: -1),
                            RGTuple(x: 3, y: -9, z: 1, w: 1)
        )
        
        let B = RGMatrix4x4(RGTuple(x: 8, y:  3, z: 7, w: 6),
                            RGTuple(x: 2, y: -1, z: 0, w:-2),
                            RGTuple(x: 2, y:  7, z: 5, w: 0),
                            RGTuple(x: 2, y:  0, z: 4, w: 5)
        )
        
        // When
        let C = A * B
        let iB = B.inverse
        // Then
        XCTAssertEqual((C * iB).isEqualTo(A), true)
        // To note the iverse(B) * C is not same as C * inverse(B)
        // To try out commen out line belowe and test.
        // XCTAssertEqual((iB * C).isEqualTo(A), true)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
