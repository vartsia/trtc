//
//  MIT License
//  Copyright (c) 2019 Juhani Nikumaa
//  vartsia.net
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
import Cocoa
import RGCore
import simd


/**
 The Ray Tracing Challenge
 Chapter 5 - Putting it together challend at the end of the chapter.
 Casting a rays at a sphere and draw the picture to a canvas.
 */

// Starting point ray at z = -5
let ray_origin = newRGPoint(x: 0, y: 0, z: -5)

// The wall is at z = 10
let wall_z = 10.0

// The wall size
let wall_size = 7.0

// Canvas size in pixels -> width and height
let canvas_pixels = 100

// Size of a pixel in world
let pixel_size = wall_size / Double(canvas_pixels)

// half => min and max x and y for the wall
let half = wall_size / 2.0


// Generate the CANVAS
// Create the canvas for the clock.
var canvas = newRGCanvas(width: canvas_pixels, height: canvas_pixels)
var color = newRGColor(red: 1.0, green: 0.0, blue: 0.0)
let shape = RGSphere()

// For each row of pixels in the canvas
for y in 0..<canvas_pixels {
    // Compute the world y coordinate (top = +half, bottom = -half)
    let world_y = half - pixel_size * Double(y)
    
    for x in 0..<canvas_pixels {
        let world_x = -half + pixel_size * Double(x)
        
        // Point on the wall
        let position = newRGPoint(x: world_x, y: world_y, z: wall_z)
        
        let ray = newRGRay(origin: ray_origin, direction: (position - ray_origin).normalize())
        let xs = intersectRGSphere(shape, ray: ray)
        
        if hitRG(intersections: xs) != nil {
            canvas.writePixelAt(RGPosition(x: x.toUInt32, y: y.toUInt32), color: color)
        }
    }
}


// Generates the ppm ASCII file string.
let ppmFile = canvasToPPMString(canvas)

// Write the canvas into a file.
let home = URL(fileURLWithPath: NSHomeDirectory())
let url = home.appendingPathComponent("Desktop/TRTC_Chapter5_Sphere.ppm")
print(url)
do {
    try ppmFile.write(to: url, atomically: false, encoding: .utf8)
} catch {
    print(error)
}
