//
//  MIT License
//  Copyright (c) 2019 Juhani Nikumaa
//  vartsia.net
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
import Cocoa
import RGCore
import simd


/**
 The Ray Tracing Challenge
 Chapter 8  - Putting it together challend at the end of the chapter.
            - This is the same program as in Chapter 7 but this one uses shadows.
 */

// 1. The floor is an extremely flattened sphere with a matte texture.
var floor = RGSphere()
floor.transform = newRGScalingMatrix(x: 10, y: 0.01, z: 10)
floor.material = newRGMaterial()
floor.material.color = newRGColor(red: 1, green: 0.9, blue: 0.9)
floor.material.specular = 0

// 2. The wall on the left has the same scale and color as the floor, but is also rotated and translated into place.
var left_wall = RGSphere()
left_wall.transform = newRGTranslationMatrix(x: 0, y: 0, z: 5) * newRGRotationY(-Double.pi/4.0) * newRGRotationX(Double.pi/2.0) * newRGScalingMatrix(x: 10, y: 0.01, z: 10)
left_wall.material = floor.material

// 3. The wall on the right is identical to the left wall, but is rotated the opposite direction in y.
var right_wall = RGSphere()
right_wall.transform = newRGTranslationMatrix(x: 0, y: 0, z: 5) * newRGRotationY(Double.pi/4.0) * newRGRotationX(Double.pi/2.0) * newRGScalingMatrix(x: 10, y: 0.01, z: 10)
right_wall.material = floor.material

// 4. The large sphere in the middle is a unit sphere, translated upward slightly and colored green.
var middle = RGSphere()
middle.transform = newRGTranslationMatrix(x: -0.5, y: 1, z: 0.5)
middle.material = newRGMaterial()
middle.material.color = newRGColor(red: 0.1, green: 1, blue: 0.5)
middle.material.diffuse = 0.7
middle.material.specular = 0.3

// 5. The smaller green sphere on the right is scaled in half.
var right = RGSphere()
right.transform = newRGTranslationMatrix(x: 1.5, y: 0.5, z: -0.5) * newRGScalingMatrix(x: 0.5, y: 0.5, z: 0.5)
right.material = newRGMaterial()
right.material.color = newRGColor(red: 0.5, green: 1, blue: 0.1)
right.material.diffuse = 0.7
right.material.specular = 0.3

// 6. The smallest sphere is scaled by a third, before being translated.
var left = RGSphere()
left.transform = newRGTranslationMatrix(x: -1.5, y: 0.33, z: -0.75) * newRGScalingMatrix(x: 0.33, y: 0.33, z: 0.33)
left.material = newRGMaterial()
left.material.color = newRGColor(red: 1, green: 0.8, blue: 0.1)
left.material.diffuse = 0.7
left.material.specular = 0.3

var world = defaultRGWorld()
world.objects.removeAll()
world.objects.append(contentsOf: [floor, left_wall, right_wall, middle, right, left])
world.lightSource = newRGPointLight(newRGColor(red: 1, green: 1, blue: 1), newRGPoint(x: -10, y: 10, z: -10))

var camera = RGCamera(hsize: 200, vsize: 100, fieldOfView: Double.pi/2)
camera.transformation = newRGViewTransformation(newRGPoint(x: 0, y: 1.5, z: -5), to: newRGPoint(x: 0, y: 1, z: 0), up: newRGVector(x: 0, y: 1, z: 0))

let canvas = renderRGWorldWithCamera(w: world, c: camera)

// Generates the ppm ASCII file string.
let ppmFile = canvasToPPMString(canvas)

// Write the canvas into a file.
let home = URL(fileURLWithPath: NSHomeDirectory())
let url = home.appendingPathComponent("Desktop/TRTC_Chapter8_Shadows.ppm")
print(url)
do {
    try ppmFile.write(to: url, atomically: false, encoding: .utf8)
} catch {
    print(error)
}

