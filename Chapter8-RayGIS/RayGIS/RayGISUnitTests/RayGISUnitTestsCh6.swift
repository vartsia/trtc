//
// RayGISUnitTestsCh6.swift
// RayGISUnitTests
//
// Created by Juhani Nikumaa on 01/06/2019
//
// MIT License
// Copyright (c) 2019 Juhani Nikumaa
// vartsia.net
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.


import XCTest
import RGCore
import simd


class RayGISUnitTestsCh6: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testComputingTheNormalOnASphere() {
        // The normal on a sphere at a point on the x axis
        // Given
        let s1 = RGSphere()
        // When
        let n1 = normalAt(s1, newRGPoint(x: 1, y: 0, z: 0))
        // Then
        XCTAssertEqual(equalTuples(n1, newRGVector(x: 1, y: 0, z: 0)), true)

        
        // The normal on a sphere at a point on the y axis
        // Given
        let s2 = RGSphere()
        // When
        let n2 = normalAt(s2, newRGPoint(x: 0, y: 1, z: 0))
        // Then
        XCTAssertEqual(equalTuples(n2, newRGVector(x: 0, y: 1, z: 0)), true)
        
        // The normal on a sphere at a point on the z axis
        // Given
        let s3 = RGSphere()
        // When
        let n3 = normalAt(s3, newRGPoint(x: 0, y: 0, z: 1))
        // Then
        XCTAssertEqual(equalTuples(n3, newRGVector(x: 0, y: 0, z: 1)), true)
        
        // The normal on a sphere at a nonaxial point
        // Given
        let s4 = RGSphere()
        // When
        let n4 = normalAt(s4, newRGPoint(x: sqrt(3)/3.0, y: sqrt(3)/3.0, z: sqrt(3)/3.0))
        // Then
        XCTAssertEqual(equalTuples(n4, newRGVector(x: sqrt(3)/3.0, y: sqrt(3)/3.0, z: sqrt(3)/3.0)), true)
    }

    
    func testNormalIsNormalized() {
        // The normal is a normalized vector
        // Given
        let s = RGSphere()
        // When
        let n = normalAt(s, newRGPoint(x: sqrt(3)/3.0, y: sqrt(3)/3.0, z: sqrt(3)/3.0))
        // Then
        XCTAssertEqual(equalTuples(n, n.normalize()), true)
    }
    
    
    func testNormalOnTransfomedShpere() {
        // Computing the normal on a translated sphere
        // Given
        var s1 = RGSphere()
        s1.transform = newRGTranslationMatrix(x: 0, y: 1, z: 0)
        // When
        let n1 = normalAt(s1, newRGPoint(x: 0, y: 1.70711, z: -0.70711))
        // Then
        XCTAssertEqual(equalTuples(n1, newRGVector(x: 0, y: 0.70711, z: -0.70711)), true)
     
        // Computing the normal on a transformed sphere
        // Given
        var s2 = RGSphere()
        let m = newRGScalingMatrix(x: 1, y: 0.5, z: 1) * newRGRotationZ(Double.pi/5.0)
        s2.transform = m
        // When
        let n2 = normalAt(s2, newRGPoint(x: 0, y: sqrt(2)/2.0, z: -sqrt(2)/2.0))
        // Then
        XCTAssertEqual(equalTuples(n2, newRGVector(x: 0, y: 0.97014, z: -0.24254)), true)
    }
    
    
    func testReflecting() {
        // Reflecting a vector approaching at 45°
        // Given
        let v1 = newRGVector(x: 1, y: -1, z: 0)
        let n1 = newRGVector(x: 0, y: 1, z: 0)
        // When
        let r1 = newRGReflect(v1, n1)
        // Then
        XCTAssertEqual(equalTuples(r1, newRGVector(x: 1, y: 1, z: 0)), true)
        
        // Reflecting a vector off a slanted surface
        // Given
        let v2 = newRGVector(x: 0, y: -1, z: 0)
        let n2 = newRGVector(x: sqrt(2)/2, y: sqrt(2)/2, z: 0)
        // When
        let r2 = newRGReflect(v2, n2)
        // Then
        XCTAssertEqual(equalTuples(r2, newRGVector(x: 1, y: 0, z: 0)), true)
    }
    
    
    func testPointLight() {
        // A point light has a position and intensity
        // Given
        let intensity = newRGColor(red: 1, green: 1, blue: 1)
        let location = newRGPoint(x: 0, y: 0, z: 0)
        // When
        let light = newRGPointLight(intensity, location)
        // Then
        XCTAssertEqual(equalColors(light.intensity, intensity), true)
        XCTAssertEqual(equalTuples(light.location, location), true)
    }
    
    
    func testMaterialCreation() {
        // The default material
        // Given
        let m = newRGMaterial()
        // Then
        XCTAssertEqual(equalColors(m.color, newRGColor(red: 1, green: 1, blue: 1)), true)
        XCTAssertEqual(m.ambient.isEqualTo(0.1), true)
        XCTAssertEqual(m.diffuse.isEqualTo(0.9), true)
        XCTAssertEqual(m.specular.isEqualTo(0.9), true)
        XCTAssertEqual(m.shininess.isEqualTo(200.0), true)
        
    }
    
    
    func testMaterialInSphere() {
        // A sphere has a default material
        // Given
        let s1 = RGSphere()
        // When
        let m1 = s1.material
        // Then
        XCTAssertEqual(m1 == newRGMaterial(), true)
        
        
        // A sphere may be assigned a material
        // Given
        var s2 = RGSphere()
        var m2 = newRGMaterial()
        m2.ambient = 1.0
        // When
        s2.material = m2
        // The
        XCTAssertEqual(s2.material == m2, true)
    }
    
    
    func testLighting() {
        let material = newRGMaterial()
        let position = newRGPoint(x: 0, y: 0, z: 0)
        // Lighting with the eye between the light and the surface
        
        // Given
        let eyev = newRGVector(x: 0, y: 0, z: -1)
        let normalv = newRGVector(x: 0, y: 0, z: -1)
        let light = newRGPointLight(newRGColorWhite(), newRGPoint(x: 0, y: 0, z: -10))
        
        // When
        let r = newRGLighting(m: material, p: position, l: light, e: eyev, n: normalv, inShadow: false)
        
        // Then
        print(r)
        XCTAssertEqual(equalColors(r, newRGColor(red: 1.9, green: 1.9, blue: 1.9)), true)
    }
    
    
    func testLightingEyeBetweenLightAndSurface() {
        let material = newRGMaterial()
        let position = newRGPoint(x: 0, y: 0, z: 0)
        // Lighting with the eye between light and surface, eye offset 45°
        
        // Given
        let eyev = newRGVector(x: 0, y: sqrt(2.0)/2.0, z: sqrt(2.0)/2.0)
        let normalv = newRGVector(x: 0, y: 0, z: -1)
        let light = newRGPointLight(newRGColorWhite(), newRGPoint(x: 0, y: 0, z: -10))
        
        // When
        let r = newRGLighting(m: material, p: position, l: light, e: eyev, n: normalv, inShadow: false)
        
        // Then
        print(r)
        XCTAssertEqual(equalColors(r, newRGColor(red: 1.0, green: 1.0, blue: 1.0)), true)
    }
    
    
    func testLightingWithEyeOpositeSurface() {
        let material = newRGMaterial()
        let position = newRGPoint(x: 0, y: 0, z: 0)
        // Lighting with eye opposite surface, light offset 45°
        
        // Given
        let eyev = newRGVector(x: 0, y: 0, z: -1.0)
        let normalv = newRGVector(x: 0, y: 0, z: -1.0)
        let light = newRGPointLight(newRGColorWhite(), newRGPoint(x: 0, y: 10, z: -10))
        
        // When
        let r = newRGLighting(m: material, p: position, l: light, e: eyev, n: normalv, inShadow: false)
        
        // Then
        print(r)
        XCTAssertEqual(equalColors(r, newRGColor(red: 0.7364, green: 0.7364, blue: 0.7364)), true)
    }
    
    
    func testLightingWithEyeOnThePath() {
        let material = newRGMaterial()
        let position = newRGPoint(x: 0, y: 0, z: 0)
        // Lighting with eye in the path of the reflection vector
        
        // Given
        let eyev = newRGVector(x: 0, y: -sqrt(2.0)/2.0, z: -sqrt(2.0)/2.0)
        let normalv = newRGVector(x: 0, y: 0, z: -1.0)
        let light = newRGPointLight(newRGColorWhite(), newRGPoint(x: 0, y: 10, z: -10))
        
        // When
        let r = newRGLighting(m: material, p: position, l: light, e: eyev, n: normalv, inShadow: false)
        
        // Then
        print(r)
        XCTAssertEqual(equalColors(r, newRGColor(red: 1.6364, green: 1.6364, blue: 1.6364)), true)
    }
    
    
    func testLightingWithLightBehind() {
        let material = newRGMaterial()
        let position = newRGPoint(x: 0, y: 0, z: 0)
        // Lighting with the light behind the surface
        
        // Given
        let eyev = newRGVector(x: 0, y: 0, z: -1)
        let normalv = newRGVector(x: 0, y: 0, z: -1)
        let light = newRGPointLight(newRGColorWhite(), newRGPoint(x: 0, y: 0, z: 10))
        
        // When
        let r = newRGLighting(m: material, p: position, l: light, e: eyev, n: normalv, inShadow: false)
        
        // Then
        print(r)
        XCTAssertEqual(equalColors(r, newRGColor(red: 0.1, green: 0.1, blue: 0.1)), true)
    }
    
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
