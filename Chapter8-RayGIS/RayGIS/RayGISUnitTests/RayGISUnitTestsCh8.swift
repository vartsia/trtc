//
//  RayGISUnitTestsCh8.swift
//  RayGISUnitTests
//
//  Created by Juhani Nikumaa on 28/08/2019.
//  Copyright © 2019 Juhani Nikumaa. All rights reserved.
//

import XCTest
import RGCore

class RayGISUnitTestsCh8: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testLightingWithSurfaceInShadow() {
        // Given
        let eyev = newRGVector(x: 0, y: 0, z: -1)
        let normalv = newRGVector(x: 0, y: 0, z: -1)
        let light = newRGPointLight(newRGColorWhite(), newRGPoint(x: 0, y: 0, z: -10))
        let inShadow = true
        let m = newRGMaterial()
        let position = newRGPoint(x: 0, y: 0, z: 0)
        
        // When
        let result = newRGLighting(m: m, p: position, l: light, e: eyev, n: normalv, inShadow: inShadow)
        
        // Then
        XCTAssertEqual(result == newRGColor(red: 0.1, green: 0.1, blue: 0.1), true)
    }
    
    func testNoShadowWhenNothingCollinearWithPoint() {
        // Given
        let w = defaultRGWorld()
        let p = newRGPoint(x: 0, y: 10, z: 0)
        
        // Then
        XCTAssertEqual(isShadowed(world: w, point: p), false)
    }
    
    func testShadowWhenObjectBetweenPointAndLight() {
        // Given
        let w = defaultRGWorld()
        let p = newRGPoint(x: 10, y: -10, z: 10)
        print(isShadowed(world: w, point: p))
        // Then
        XCTAssertEqual(isShadowed(world: w, point: p), true)
    }
    
    func testNoShadowWhenObjectBehindTheLight() {
        // Given
        let w = defaultRGWorld()
        let p = newRGPoint(x: -20, y: 20, z: -20)
        
        // Then
        XCTAssertEqual(isShadowed(world: w, point: p), false)
    }
    
    func testNoShadowWhenObjectBehindThePoint() {
        // Given
        let w = defaultRGWorld()
        let p = newRGPoint(x: -2, y: 2, z: -2)
        
        // Then
        XCTAssertEqual(isShadowed(world: w, point: p), false)
    }

    func testShadeHit() {
        // Given
        var w = RGWorld()
        w.lightSource = newRGPointLight(newRGColor(red: 1, green: 1, blue: 1), newRGPoint(x: 0, y: 0, z: -10))
        let s1 = RGSphere()
        w.objects.append(s1)
        var s2 = RGSphere()
        s2.transform = newRGTranslationMatrix(x: 0, y: 0, z: 10)
        w.objects.append(s2)
        let r = newRGRay(origin: newRGPoint(x: 0, y: 0, z: 5), direction: newRGVector(x: 0, y: 0, z: 1))
        let i = RGIntersection(t: 4, object: s2)
        
        // When
        let comp = prepareRGComputation(intersection: i, ray: r)
        let c = newRGShadeHit(world: w, computations: comp)
        
        // Then
        XCTAssertEqual(c == newRGColor(red: 0.1, green: 0.1, blue: 0.1), true)
    }
    
    func testHitShouldOffsetThePoint() {
        // Given
        let r = newRGRay(origin: newRGPoint(x: 0, y: 0, z: -5), direction: newRGVector(x: 0, y: 0, z: 1))
        var s = RGSphere()
        s.transform = newRGTranslationMatrix(x: 0, y: 0, z: 1)
        let i = RGIntersection(t: 5, object: s)
        
        // When
        let comps = prepareRGComputation(intersection: i, ray: r)
        
        // Then
        XCTAssertEqual(comps.overPoint.z < -Double.eps()/2.0, true)
    }
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
