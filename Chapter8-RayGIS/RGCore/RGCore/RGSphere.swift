//
// RGSphere.swift
// RGCore
//
// Created by Juhani Nikumaa on 11/05/2019
//
// MIT License
// Copyright (c) 2019 Juhani Nikumaa
// vartsia.net
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.


import Foundation
import simd


public struct RGSphere: Equatable {
    public let id = NSUUID.init().uuidString
    public var origin = newRGPoint(x: 0, y: 0, z: 0)
    public var radius: Double = 1.0
    public var transform: RGMatrix4x4 = RGMatrix4x4.identity()
    public var material: RGMaterial = newRGMaterial()
    
    public init() {
    }
    
    
    /**
     Implements the Equatable protocol.
     We do not use the id since we just wan't to know if objects are equal in terms of data.
     To check if objects are the same just chek the id.
     */
    public static func == (l: RGSphere, r: RGSphere) -> Bool {
        return
            equalTuples(l.origin, r.origin) &&
            l.radius.isEqualTo(l.radius) &&
            l.transform.isEqualTo(r.transform) &&
            l.material == r.material
    }
}
