//
// RGMaterial.swift
// RGCore
//
// Created by Juhani Nikumaa on 03/06/2019
//
// MIT License
// Copyright (c) 2019 Juhani Nikumaa
// vartsia.net
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.


import Foundation
import simd


public struct RGMaterial {
    public var color: RGColor
    public var ambient: Double
    public var diffuse: Double
    public var specular: Double
    public var shininess: Double
}


public func newRGMaterial() -> RGMaterial {
    return RGMaterial(color: newRGColor(red: 1, green: 1, blue: 1), ambient: 0.1, diffuse: 0.9, specular: 0.9, shininess: 200.0)
}


extension RGMaterial: Equatable {
    static public func == (lhs: RGMaterial, rhs: RGMaterial) -> Bool {
        return
            lhs.ambient.isEqualTo(rhs.ambient) &&
            equalColors(lhs.color, rhs.color) &&
            lhs.diffuse.isEqualTo(rhs.diffuse) &&
            lhs.shininess.isEqualTo(rhs.shininess) &&
            lhs.specular.isEqualTo(rhs.specular)
    }
}
