//
// RGMatrixTransformations.swift
// RGCore
//
// Created by Juhani Nikumaa on 14/04/2019
//
// MIT License
// Copyright (c) 2019 Juhani Nikumaa
// vartsia.net
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.


import Foundation
import simd


/**
 Generates new RGMatrix4x4 translation matrix.
 ````
 |0|1|2|3|   <- Columns
 ____________
 |1|0|0|x|
 |0|1|0|y|
 |0|0|1|z|
 |0|0|0|1|
 ````
 */
public func newRGTranslationMatrix(_ translation: double3) -> RGMatrix4x4 {
    var tM = RGMatrix4x4.identity()
    tM[3] = RGTuple(x: translation.x, y: translation.y, z: translation.z, w: 1.0)
    return tM
}


/**
 Alternative way to create translation matrix using direct scalar values.
 Generates new RGMatrix4x4 translation matrix.
 */
public func newRGTranslationMatrix(x: Double, y: Double, z: Double) -> RGMatrix4x4 {
    var tM = RGMatrix4x4.identity()
    tM[3] = RGTuple(x: x, y: y, z: z, w: 1.0)
    return tM
}


public func newRGScalingMatrix(_ scaling: double3) -> RGMatrix4x4 {
    var sM = RGMatrix4x4.identity()
    
    sM[0].x = scaling.x
    sM[1].y = scaling.y
    sM[2].z = scaling.z
    
    return sM
}


/**
 Generates new RGMatrix4x4 scaling matrix.
 ````
 |0|1|2|3|   <- Columns
 ____________
 |x|0|0|0|
 |0|y|0|0|
 |0|0|z|0|
 |0|0|0|1|
 ````
 Takes scaling farctor for each coordinate axe separately.
 */
public func newRGScalingMatrix(x: Double, y: Double, z: Double) -> RGMatrix4x4 {
    var sM = RGMatrix4x4.identity()
    
    sM[0].x = x
    sM[1].y = y
    sM[2].z = z
    
    return sM
}


/**
 Generates new RGMatrix4x4 rotation matrix arond X axis.
 ````
 |0|1|2|3|   <- Columns
 ____________
 |1|     0|      0|0|
 |0|cos(r)|-sin(r)|0|
 |0|sin(r)| cos(r)|0|
 |0|     0|      0|1|
 ````
 Takes rotation angle as radians.
 */
public func newRGRotationX(_ rad: Double) -> RGMatrix4x4 {
    var rotXm = RGMatrix4x4.identity()
    
    rotXm[1].y = cos(rad)
    rotXm[1].z = sin(rad)
    rotXm[2].y = -sin(rad)
    rotXm[2].z = cos(rad)
    
    return rotXm
}


/**
 Generates new RGMatrix4x4 rotation matrix arond Y axis.
 ````
 |0|1|2|3|   <- Columns
 ____________
 |cos(r) |0|sin(r)|0|
 |0      |1|     0|0|
 |-sin(r)|0|cos(r)|0|
 |      0|0|     0|1|
 ````
 Takes rotation angle as radians.
 */
public func newRGRotationY(_ rad: Double) -> RGMatrix4x4 {
    var rotYm = RGMatrix4x4.identity()
    
    rotYm[0].x = cos(rad)
    rotYm[0].z = -sin(rad)
    rotYm[2].x = sin(rad)
    rotYm[2].z = cos(rad)
    
    return rotYm
}



/**
 Generates new RGMatrix4x4 rotation matrix arond Z axis.
 ````
 |0|1|2|3|   <- Columns
 ____________
 |cos(r) |-sin(r)|0|0|
 |sin(r) | cos(r)|0|0|
 |      0|      1|  0|
 |      0|      0|  1|
 ````
 Takes rotation angle as radians.
 */
public func newRGRotationZ(_ rad: Double) -> RGMatrix4x4 {
    var rotZm = RGMatrix4x4.identity()
    
    rotZm[0].x = cos(rad)
    rotZm[0].y = sin(rad)
    rotZm[1].x = -sin(rad)
    rotZm[1].y = cos(rad)
    
    return rotZm
}


/**
 Generates new RGMatrix4x4 rotation matrix arond Y axis.
 ````
 |0 |1 |2 |3|   <- Columns
 ____________
 |1 |Xy|Xz|0|
 |Yx| 1|Yz|0|
 |Zx|Zy| 1|0|
 |0 | 0| 0|1|
 ````
 Takes rotation angle as radians.
 */
public func newRGShearing(_ Xy: Double, _ Xz: Double, _ Yx: Double, _ Yz: Double, _ Zx: Double, _ Zy: Double ) -> RGMatrix4x4 {
    var sherM = RGMatrix4x4.identity()
    
    sherM[0].y = Yx
    sherM[0].z = Zx
    sherM[1].x = Xy
    sherM[1].z = Zy
    sherM[2].x = Xz
    sherM[2].y = Yz
    
    return sherM
}


/**
 Generates new RGViewTransformation matrix.
 ````
 |0     |1       |2         |3|   <- Columns
 ______________________________
 |left.x|trueUp.x|-forward.x|0|
 |left.y|trueUp.y|-forward.y|0|
 |left.z|trueUp.y|-forward.z|0|
 |0     |       0|          |1|
 ````
 Takes rotation angle as radians.
 */
public func newRGViewTransformation(_ from: RGTuple, to: RGTuple, up: RGTuple) -> RGMatrix4x4 {
    
    let forward = (to - from).normalize()
    let nup = up.normalize()
    let left = cross(lT: forward, rT: nup)
    let trueUp = cross(lT: left, rT: forward)
    
    var orientation = RGMatrix4x4.identity()
    orientation[0].x = left.x
    orientation[0].y = trueUp.x
    orientation[0].z = -forward.x
    orientation[0].w = 0
    
    orientation[1].x = left.y
    orientation[1].y = trueUp.y
    orientation[1].z = -forward.y
    orientation[1].w = 0
    
    orientation[2].x = left.z
    orientation[2].y = trueUp.z
    orientation[2].z = -forward.z
    orientation[2].w = 0
    
    print(orientation)
    return orientation * newRGTranslationMatrix(x: -from.x, y: -from.y, z: -from.z)
}
