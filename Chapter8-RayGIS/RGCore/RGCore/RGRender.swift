//
//  RGRender.swift
//  RGCore
//
//  Created by Juhani Nikumaa on 20/08/2019.
//  Copyright © 2019 Juhani Nikumaa. All rights reserved.
//

import Foundation
import simd

public func renderRGWorldWithCamera(w: RGWorld, c: RGCamera) -> RGCanvas {
    var canvas = RGCanvas(size: RGSize(width: c.hSize, height: c.vSize))
    
    for y in 0..<c.vSize {
        for x in 0..<c.hSize {
            let r = newRGRayForPixel(c: c, x: x, y: y)
            let color = RGColorAt(world: w, ray: r)
            _ = writePixelToRGCanvas(&canvas, position: RGPosition(x: x.toUInt32, y: y.toUInt32), color: color)
        }
    }
    
    return canvas
}
