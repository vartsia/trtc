//
// RGComputation.swift
// RGCore
//
// Created by Juhani Nikumaa on 04/08/2019
//
// MIT License
// Copyright (c) 2019 Juhani Nikumaa
// vartsia.net
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.


import Foundation


/**
 Datastucture to hold attributes in one place to make it easier to do the calculations.
 */
public struct RGComputation {
    public var t: Double
    public var object: RGSphere
    public var point: RGTuple
    public var eyev: RGTuple
    public var normal: RGTuple
    public var inside: Bool
    public var overPoint: RGTuple
}


public func prepareRGComputation(intersection i: RGIntersection, ray: RGRay) -> RGComputation {
    let point = newRGPosition(ray: ray, t: i.t)
    
    let eye = -ray.direction
    var normal = normalAt(i.object, point)
    var inside = false
    
    if dot(lT: normal, rT: eye) < 0 {
        inside = true
        normal = -normal
        
    }
    
    let overPoint = point + normal * Double.eps()
    let comps = RGComputation(t: i.t , object: i.object, point: point, eyev: -ray.direction, normal: normal, inside: inside, overPoint: overPoint)
    
    return comps
}


/**
 - Returns: RGColor at the intersection encapsulated by computations in given world.
 */
public func newRGShadeHit(world: RGWorld, computations comps: RGComputation) -> RGColor {
    let shadowed = isShadowed(world: world, point: comps.overPoint)
    
    return newRGLighting(m: comps.object.material , p: comps.point, l: world.lightSource!, e: comps.eyev, n: comps.normal, inShadow: shadowed)
}
