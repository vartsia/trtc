//
//  RGShadow.swift
//  RGCore
//
//  Created by Juhani Nikumaa on 29/08/2019.
//  Copyright © 2019 Juhani Nikumaa. All rights reserved.
//

import Foundation
import simd

/**
 Calculates if given point in a world is in shadow or not.
 This information is used in newRGLighting function.
 */
public func isShadowed(world: RGWorld, point: RGTuple) -> Bool {
    
    let v = world.lightSource!.location - point
    let distance = v.magnitude()
    let direction = v.normalize()
    let ray = newRGRay(origin: point, direction: direction)
    let intersections = intersectRGWorld(world: world, ray: ray)
    
    guard let h = hitRG(intersections: intersections) else {
        return false
    }

    if h.t.isLess(than: distance) {
        return true
    } else {
        return false
    }
}
