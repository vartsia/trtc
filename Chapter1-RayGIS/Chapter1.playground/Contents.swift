//
//  MIT License
//  Copyright (c) 2019 Juhani Nikumaa
//  vartsia.net
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.


import Cocoa
import RGCore


/**
 The Ray Tracing Challenge
 Chapter 1 - Putting it together challend at the end of the chapter.
 */


struct Projectile {
    var position: RGTuple
    var velocity: RGTuple
}


struct Environment {
    var gravity: RGTuple
    var wind: RGTuple
}


func tick(environment: Environment, projectile: Projectile) -> Projectile {
    let pos = projectile.position + projectile.velocity
    let vel = projectile.velocity + environment.gravity + environment.wind
    
    return Projectile(position: pos, velocity: vel)
}


/*
 projectile starts one unit above the origin.
 velocity is normalized to 1 unit/tick.
 */


var p = Projectile(position: newPoint(x: 0, y: 1, z: 0), velocity: newVector(x: 1, y: 1, z: 0).normalize())
var e = Environment(gravity: newVector(x: 0, y: -0.1, z: 0), wind: newVector(x: -0.01, y: 0, z: 0))


while p.position.y > 0.0 {
    p = tick(environment: e, projectile: p)
    let y = p.position.y
    print("y = \(y)")
}
